---
layout: page
title: Resources
categories: resources
permalink: resources/
published: false
---

> It can be difficult to find good references. Below is a collection of textbooks, papers, etc. that I have found to be particularly effective in communicating their material.


### Wireless Communication

<ul>
{% for b in site.data.books.wireless %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>


### Linear Algebra

<ul>
{% for b in site.data.books.linalg %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>


### Optimization

<ul>
{% for b in site.data.books.optimization %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>


### Full-Duplex

<ul>
{% for b in site.data.books.fullduplex %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>


### Compressed Sensing

<ul>
{% for b in site.data.books.cs %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>


### Other

<ul>
{% for b in site.data.books.other %}
  <li><a href="{{ b.link }}" target="_blank">{{ b.title }}</a> by {{ b.author }}</li>
{% endfor %}
</ul>
