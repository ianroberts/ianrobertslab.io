---
layout: project
title:  "Digital Self-Interference Cancellation for In-Band Full-Duplex Radios"
when: "Fall 2018"
date: 2018-09-01
summary: "Researched and implemented state-of-the-art digital self-interference cancellation methods to estimate and cancel both linear and nonlinear self-interference components. Methods implemented include linear and nonlinear LMS channel estimation and a feedforward neural network."
pdf: "/pdf/dsic.pdf"
---

> {{ page.summary }} [<a href="{{ page.pdf }}">pdf</a>]

In recent years, there has been significant progress in realizing in-band full-duplex, the capability for a radio to simultaneously transmit and receive on the same channel (e.g., time and frequency). What has traditionally prevented the realization of in-band full-duplex is the undesired self-interference between a radio's transmitter and its receiver which corrupts a desired receive signal, forcing it to use time or frequency division duplexing to avoid such interference. In order for a full-duplex radio to reliably receive a desired signal while transmitting, this self-interference must be dealt with by what is known as self-interference cancellation which typically occurs in two stages: analog cancellation and digital cancellation.

With the growing number of wireless devices, spectrum availability is becoming more scarce while the demand for higher data rates and more spectrum increases. In-band full-duplex (FD) has shown promise in satisfying these demands by enabling simultaneous transmission and reception over the same frequency band, eliminating the need for FDD or TDD thus increasing the spectral efficiency. Perhaps the most attractive feature of a FD radio is this increased spectral efficiency which can theoretically be up to twice that of an equivalent half-duplex radio since transmission and reception occur over the same time-frequency resource. In addition to commercial wireless communication, the applications of FD extend to military communication and radar as well as consumer cable television and internet. Further, along with the increased spectral efficiency, FD offers MAC and network gains by suggesting new means of cooperation between devices. 

What has long made it difficult to achieve FD is the undesired coupling between a radio's TX and its RX during simultaneous transmission and reception in what is referred to as self-interference (SI). Since a desired receive signal, or SoI, is likely to be very weak due to the loss incurred in its travel, the SI power is often many orders of magnitude higher than that of the SoI. Therefore, in order to reliably detect and demodulate the SoI, the SI must be removed from the total received signal by what is known as SIC. In fact, to offer a SoI the same SINR as an equivalent half-duplex system, a FD radio must cancel the SI completely to the thermal noise floor of its RX which may require 100 dB or more of total SIC.

![Linear and nonlinear DSIC](/images/dsic/linear-nonlinear-dsic.svg#center, "Linear and nonlinear DSIC")

The figure above shows the results of two forms of DSIC. Observe the strength of the SI which would corrupt a desired receive signal. Linear DSIC can be seen to cancel 31.7 dB of the SI. Nonlinear DSIC is even more effective, achieving 37.3 dB of SIC. The difference between linear and nonlinear cancellation is the complexity of the basis function used in estimation and SI reconstruction. Linear DSIC uses a simple model which assumes only linear components, while nonlinear DSIC considers nonlinearities in the SI signal, making it a much more complicated model. Thus, there is typically a tradeoff made in computational complexity and provided amount of SIC.