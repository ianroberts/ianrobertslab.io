---
layout: project
title:  "Beamforming Cancellation Design for Millimeter-Wave Full-Duplex"
when: "Spring 2019"
date: 2019-04-01
summary: "In this project, I propose two beamforming cancellation designs for millimeter-wave systems employing hybrid beamforming. My designs sufficiently suppress the incurred self-interference during full-duplex operation achieving simultaneous transmission and reception at near ideal full-duplex performance."
pdf: "/pdf/bfc.pdf"
---

> {{ page.summary }} [<a href="{{ page.pdf }}">pdf</a>]

In recent years, there has been extensive research
on millimeter-wave (mmWave) communication and on in-band
full-duplex (FD) communication, but work on the combination
of the two is relatively lacking. FD mmWave systems could offer
increased spectral efficiency and decreased latency while also
suggesting the redesign of existing mmWave applications. While
FD technology has been well-explored for sub-6 GHz systems, the
developed methods do not translate well to mmWave, turning
researchers to a method known as beamforming cancellation
(BFC), where the highly directional mmWave beams are steered
to mitigate self-interference (SI) and enable simultaneous transmission 
and reception. In this paper, we present a BFC method
for two hybrid architectures that can sufficiently suppress the
SI such that the sum spectral efficiency is near that of a SI-free
FD system. A simulation and its results are used to verify our
design.

The recent work on millimeter-wave (mmWave) communication has 
enabled it to become a core technology of future
fifth generation (5G) cellular networks. The wide bandwidths
available at mmWave frequencies make it attractive solution
to offer higher rates and serving more users, but there exist
many significant challenges with communication at such a
high center frequency. As result of its high path loss,
mmWave communication necessitates the use of dense antenna
arrays to provide sufficient beamforming gains to close the link
and enough margin for wide-area use.

In mmWave multiple-input multiple-output (MIMO), it is
not practical to have a dedicated radio frequency (RF) chain
for each array element due to the associated financial cost,
power consumption, size, and complexity. Thus, hybrid
analog/digital beamforming is often used where the combination 
of a baseband (digital) beamformer and an RF (analog)
beamformer are used to achieve comparable performance
while reducing the number of RF chains. 
A mmWave communication block diagram is shown below, 
where fully-connected hybrid beamforming is employed, 
incorporating both analog and digital processing.

![Hybrid beamforming block diagram.](/images/mmwave/mmwave_hybrid_beamforming_block.svg#center, "Hybrid beamforming block diagram.")

First introducing itself about a decade ago is the practical
realization of in-band full-duplex (FD) communication
where transmission and reception occur using the same time-
frequency resource. Such a capability offers exciting
potential due to its possible doubling of spectral efficiency and
its opportunity to reimagine applications that use conventional
half-duplex (HD) techniques. To name a few, cognitive radio,
relay nodes, and medium access control (MAC) are areas
where the ability to simultaneously transmit and receive can
introduce gains aside from increased spectral efficiency. When
attempting to receive while transmitting, a device incurs self-
interference (SI) which corrupts the desired receive signal if
not dealt with. In order to achieve FD, researchers have developed 
various self-interference cancellation (SIC) methods,
which take advantage of the fact that a transceiver is privy
to its transmitted signal, enabling it to reconstruct the SI and
subtract it from the corrupted signal leaving the desired receive
signal nearly interference-free.

The majority of existing work on FD, however, is in its
application to sub-6 GHz systems. With the rise of mmWave
and its use in 5G, the extension of FD to mmWave has
come to interest. However, mmWave FD cannot be realized
by simply applying the SIC methods developed for sub-6 GHz
systems. This is largely due to the numerous antennas in use,
wide bandwidths, increased phase noise, and highly nonlinear
components at mmWave.

Thus, the means by which researchers have proposed
achieving mmWave FD is referred to as beamforming cancellation 
(BFC) where the highly directional mmWave beams
are used to mitigate SI by strategic steering. When
operating in FD at mmWave, both a transmit and receive
beamformer will be in use simultaneously and will operate
over the same frequencies. The goal of BFC is to steer the
transmit and receive beamformers such that they mitigate the
SI while transmitting and receiving from the desired user(s).
Without BFC, the SI strength will likely be many orders of
magnitude stronger than the incoming desired receive signal,
meaning the beamformers must be designed to sufficiently
mitigate the SI power while still providing sufficient gain
on the desired links for successful communication. These
requirements are especially true given that practical mmWave
communication is typically at low signal-to-noise ratio (SNR).
The hybrid architectures used at mmWave complicate BFC
due to the limitations of the analog beamformer and a desirably
low number of RF chains. In this paper, we present two BFC
designs where FD communication can be achieved at mmWave
when two different hybrid beamforming architectures are employed. 
We present the results of simulation which validate our
designs, both of which show that performance can meet near
ideal FD operation which vastly surpasses using the eigen-
beamformers.

Existing mmWave FD designs are quite lacking. Some
designs, for example, do not consider SI at all in their
design. The few existing BFC designs that consider SI
are iterative approaches that rely on convergence to a locally
optimum solution and involve joint design across transmitter
and receiver pairs. Our design, on the other hand,
is not iterative and does not rely on a joint design across
nodes. Additionally, our methods consider the hybrid structure
and its limitations during design whereas existing approaches
assume fully-digital beamforming and decompose into hybrid approximations 
after the fact, not necessarily considering
limitations such as phase shifter resolution.