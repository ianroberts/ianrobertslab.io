---
layout: project
title:  "Enabling In-Band Coexistence of Millimeter-Wave Communication and Radar"
when: "Fall 2019"
date: 2019-11-01
summary: "In this project, I present a beamforming design that enables in-band coexistence of a millimeter-wave communication system and radar."
pdf: "/pdf/radar2020.pdf"
---

> {{ page.summary }} [<a href="{{ page.pdf }}">pdf</a>]

The wide bandwidths available at millimeter-wave (mmWave) frequencies have offered exciting potential to wireless communication systems and radar alike. Communication systems can offer higher rates and support more users with mmWave bands while radar systems can benefit from higher resolution captures. This leads to the possibility that portions of mmWave spectrum will be occupied by both communication and radar (e.g., 60 GHz industrial, scientific, and medical (ISM) band). This potential coexistence motivates the work of this paper, in which we present a design that can enable simultaneous, in-band operation of a communication system and radar system across the same mmWave frequencies. To enable such a feat, we mitigate the interference that would otherwise be incurred by leveraging the numerous antennas offered in mmWave communication systems. Dense antenna arrays allow us to avoid interference spatially, even with the hybrid beamforming constraints often imposed by mmWave communication systems. Simulation shows that our design sufficiently enables simultaneous, in-band coexistence of a mmWave radar and communication system.