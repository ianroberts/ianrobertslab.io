---
layout: project
title:  "Frequency-Selective Beamforming Cancellation Design for Millimeter-Wave Full-Duplex"
when: "Fall 2019"
date: 2019-09-01
summary: "In this project, I propose a beamforming design for hybrid beamforming millimeter-wave systems communicating on frequency-selective channels. My designs suppress the incurred self-interference during full-duplex operation achieving simultaneous transmission and reception in-band."
pdf: "/pdf/fsbfc.pdf"
---

> {{ page.summary }} [<a href="{{ page.pdf }}">pdf</a>]

The wide bandwidths offered at millimeter-wave (mmWave) frequencies have made them an attractive choice for future wireless communication systems. Recent works have presented beamforming strategies for enabling in-band full-duplex (FD) capability at mmWave even under the constraints of hybrid beamforming, extending the exciting possibilities of next-generation wireless. Existing mmWave FD designs, however, do not consider frequency-selective mmWave channels. Wideband communication at mmWave suggests that frequency-selectivity will likely be of concern since communication channels will be on the order of hundreds of megahertz or more. This has motivated the work of this paper, in which we present a frequency-selective beamforming design to enable practical wideband mmWave FD applications. In our designs, we account for the challenges associated with hybrid analog/digital beamforming such as phase shifter resolution, a desirably low number of radio frequency (RF) chains, and the frequency-flat nature of analog beamformers. We use simulation to validate our work, which indicates that spectral efficiency gains can be achieved with our design by enabling simultaneous transmission and reception in-band.