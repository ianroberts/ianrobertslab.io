---
layout: project
title:  "The Effect of User Demands on Millimeter-Wave Full-Duplex Requirements"
when: "Spring 2019"
date: 2019-01-01
summary: "I have analyzed the spectral efficiencies offered when performing beamforming cancellation at a full-duplex base-station communicating to two half-duplex users. From this, we can determine regions where beamforming cancellation can offer sufficient self-interference mitigation such that operating at full-duplex is worthwhile over half-duplex. This is then extended to incorporate user demands."
pdf: "/pdf/userdemandsbfc.pdf"
---

> {{ page.summary }} [<a href="{{ page.pdf }}">pdf</a>]

Let's consider the case where two half-duplex (HD) nodes are connected to a full-duplex (FD) node as depicted in the figure below. As shown, the FD node $$(i)$$ is transmitting downlink (DL) to $$(j)$$ while receiving uplink (UL) from $$(k)$$ on the same time-frequency resource at millimeter-wave (mmWave).

![Two nodes connected to a full-duplex base station.](/images/mmwave/nodes.svg#center, "Two nodes connected to a full-duplex base station")

Since $$(i)$$ is transmitting while receiving, it incurs self-interference (SI) which degrades the signal quality of the UL from $$(k)$$ if not dealt with. While mmWave links are typically highly directional, their directionality is not sufficient to inherently mitigate the SI. With purely eigen-beamformers, for example, the SI is too strong to be ignored. Therefore, we turn to a method for mitigating the SI known as beamforming cancellation (BFC) where the highly directional mmWave beams are strategically steered such that they suppress the SI. 

In performing BFC, our transmit and receive beamformers at $$(i)$$ will deviate from their eigen-beamformers so there will be some loss on their respective links. It is the goal that this loss is marginal and is outweighed by the gains had from operating in FD rather than HD. Thus, BFC design will involve mitigating the SI while not degrading its desired links to $$(j)$$ and $$(k)$$. In fact, if the BFC degrades the links too much or does not suppress the SI enough, it may be that operating in FD is infereior to HD, since HD faces no SI and not link degradation. In this project, we explore the requirements of BFC design such that operating in FD is superior to HD. 

For these results, we assume users are duplexed using TDMA equally and their UL and DL are also equal. Thus, operating in FD could offer both users twice the spectral efficiency on their UL and DL since they are allocated twice the amount of time.

In the plot below, it is seen that if sufficient SI suppression is not met (SIR of 10 dB), even with no link degradation, operating in HD is superior to FD. The reason for this is simple. If the HD links have a high SNR, they will have a high spectral efficiency. During FD operation, if the SI is not suppressed enough, the interference will degrade the receive (UL) link past the point where the FD pre-log factor does not make up for the difference. The DL is not affected by SI, so its spectral effiency during FD is double of that during HD (when no link degradation is considered).

![Caption here.](/images/mmwave/sir10.svg#center, "Spectral effiencies offered to $$(j)$$ when SIR is 10 dB and without any link degradation.")

Similar relationships can be seen in the <a href="{{ page.pdf }}">slide deck</a>. 

The main takeaway of this project is that BFC design should meet the criteria of SI suppresssion and beamforming gain such that operating in FD is superior to HD. Note that this analysis relies on the service of two users and the conclusions drawn can not be applied directly to the case when more users are being served by a FD base-station. As one could imagine, introducing more users to the network give the base-station more flexibility in how it serves those users, so requirements for BFC design may not be quite as strict as in the case when only two users are present. 

Additionally, it should be noted that the FD node does not have to operate in FD mode all the time. Consider the two user case as shown above. If user $$(j)$$ is served and has no more data to send, node $$(i)$$ could switch to HD operation and serve $$(k)$$ at a rate free of interference and link degradation.