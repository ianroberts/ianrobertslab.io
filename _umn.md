---
layout: page
title: University of Minnesota, Twin Cities
categories: umn
permalink: umn/
hide_in_nav: true
---

> Please see below for materials related to my faculty application at the University of Minnesota.

![UMN](/pdf/umn/logo.png#center, "UMN")

### Useful Links

[Faculty Talk Slides](/pdf/umn/slides.pdf)

[Full CV](/pdf/umn/cv.pdf)

[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)

[Publications Page](/publications)

[MIMO for MATLAB (a software package)](https://mimoformatlab.com)



### Publications

Copies of some of my papers are available upon request. They cannot be released publicly due to limited release restrictions imposed by industry collaborators until the paper's acceptance/publication.

{% assign pubs = (site.data.publications | sort: 'date') | reverse %}

#### Preprint:
{% for p in pubs %}
{% if p.type == 'preprint' %}
{% if p.pdf %} - ({{ p.date | date: "%Y" }}) [{{ p.title }}]({{ p.pdf }}){% else %}- ({{ p.date | date: "%Y" }}) {{ p.title }} (limited release){% endif %}
{% endif %}
{% endfor %} 


#### Journal/Magazine:
{% for p in pubs %}
{% if p.type == 'journal' %}
{% if p.pdf %} - ({{ p.date | date: "%Y" }}) [{{ p.title }}]({{ p.pdf }}){% endif %}
{% endif %}
{% endfor %}


#### Conference:
{% for p in pubs %}
{% if p.type == 'conference' %}
{% if p.pdf %} - ({{ p.date | date: "%Y" }}) [{{ p.title }}]({{ p.pdf }}){% else %}- ({{ p.date | date: "%Y" }}) {{ p.title }} (limited release){% endif %}
{% endif %}
{% endfor %} 
 



