---
layout: page
title: Projects
categories: projects
permalink: projects/
---

> Brief summaries of some of the projects I have worked on in my courses, in my research, and on my own.

{% for project in site.projects reversed %}
**<a href="{{ project.url }}">{{ project.title }}</a>**, _{{ project.when }}_  
{{ project.summary }}  

{% endfor %}