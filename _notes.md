---
layout: page
title: Notes
categories: notes
permalink: notes/
---

> Posts on various topics that may help you learn something technically or otherwise.

### Most Recent
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'new' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

### Wireless
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'wireless' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

### LaTeX
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'latex' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>


### Other
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'other' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>
