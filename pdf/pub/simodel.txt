@ARTICLE{ipr_simodel,
    author={I. P. Roberts and A. Chopra and T. Novlan and S. Vishwanath and J. G. Andrews},
    title={Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference},
    year=2023,
    month={Jul.},
    journal={IEEE J. Sel. Areas Commun.},
    note={(early access)},
}

@ARTICLE{ipr_simodel,
    author={I. P. Roberts and A. Chopra and T. Novlan and S. Vishwanath and J. G. Andrews},
    title={Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference},
    year=2023,
    month=jul,
    journal=IEEE_J_SAC,
    note={(early access)},
}
