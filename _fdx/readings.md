---
layout: page
title: "Readings in Millimeter Wave Full-Duplex"
summary: A collection of recommended papers and resources in mmWave full-duplex literature.
date: 2021-11-10
published: true
---

> {{ page.summary }}

All sections are sorted chronologically, starting with most recent. A list of all readings can be found at the bottom of the page.

If you have any readings you would like me to (anonymously) include on this page, please fill out this [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSfwmLBmk73Y4Q-tfprTOeHwIKdaEGi-EIzIgpfS8FFDrUaW0Q/viewform?usp=sf_link). 

[//]: # test


### Overviews and Foundations

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'overview' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


[//]: ### Channel Measurements and Modeling

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'channel' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


### Measurements and Prototyping

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'prototype' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


### Hybrid and Analog Beamforming Solutions

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'beamforming' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


### Digital Self-Interference Cancellation Solutions

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'dsic' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


### Analog Self-Interference Cancellation Solutions

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
{% if p.categories contains 'asic' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endif %}
{% endfor %}


### All Readings

{% assign n = (site.data.readings_fdx | sort: 'date') | reverse %}
{% for p in n %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%B %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>)<br>{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.acm %}[**[ACM]**]({{ p.acm }}) {% endif %}{% if p.sensors %}[**[Sensors]**]({{ p.sensors }}) {% endif %}  
<br>
{% endfor %}
