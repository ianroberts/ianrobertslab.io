---
layout: page
title: "2023 Full-Duplex Forum"
summary: An introduction to in-band full-duplex.
categories: [wireless]
date: 2023-06-24
published: false
---

**When: September XX, 2023 at 10 AM EST**

**Where: Zoom**

**RSVP: Link**

The Full-Duplex Forum aims to gather researchers from academia and industry to present and discuss recent progress and future directions on topics related to full-duplex technology.

The goals of the Full-Duplex Forum are: 
- to share recent research progress on full-duplex with one another;
- to increase the synergy and collaboration within the full-duplex research community;
- to identify pressing technical challenges and steer future research on full-duplex.

The 2023 Full-Duplex Forum will be held over Zoom on September XX, 2023.

Attendance to the forum is open to the public. Please RSVP here. 

Presenters for the forum will be by invitation only.

### Agenda

Times listed below are in EST.

- 10:00 -- 10:15 AM: **Welcome and Introduction**  
  Ian P. Roberts, UCLA
- 10:20 -- 10:35 AM: **Talk Title**  
  Speaker, Affiliation
- 10:40 -- 10:55 AM: **Talk Title**  
  Speaker, Affiliation
- 11:00 -- 11:15 AM: **Talk Title**  
  Speaker, Affiliation
- 11:20 -- 11:35 AM: **Talk Title**  
  Speaker, Affiliation
- 11:40 -- 11:55 AM: **Talk Title**  
  Speaker, Affiliation
- 12:00 -- 12:30 AM: **Panel**  
  Speaker 1, Affiliation  
  Speaker 2, Affiliation  
  Speaker 3, Affiliation  
  Moderator: Name, Affiliation

  
[//]: # Ian, Ken, George C. Alexandropoulos, Alexios Balatsoukas-Stimming, 


[//]: # Yun Liao, Besma, Dani Korpi, Asil Koc, Ashu, Himal, CBC, Gabor, GYS, Jongwoo, Rajesh, Hardik, Farzad, 
