---
layout: page
title: "Translating Existing Full-Duplex Solutions to Millimeter Wave Systems"
summary: An outline of the complications associated with translating existing self-interference cancellation solutions to mmWave systems.
date: 2021-11-24
published: true
---

> {{ page.summary }}


Analog self-interference cancellation (SIC), where a tunable filter is placed between the transmitter and receiver, is a common full-duplex solution in sub-6 GHz radios.

{% include image.html file="fdx/fdx_sub6_siso_asic.svg" width=70 caption="Analog self-interference cancellation aims to reconstruct and cancel self-interference in hardware." link=false %}

Translating this solution to millimeter wave systems is not straightforward practically due to the dense antenna arrays that are used by millimeter wave systems.
For example, with 256 antenna elements at the transmitter and 256 elements at the receiver of the full-duplex device, over 65,000 coupling paths manifest between the transmit array and receive array.
An analog SIC attempting to cancel these many sources of self-interference would grow prohibitively large and complex.

{% include image.html file="fdx/fdx_mmwave_asic_antenna_type_01.svg" width=75 caption="Translating per-antenna analog SIC to mmWave systems is not practical due to the number of antennas." link=false %}

Other approaches, like digital SIC, have promise at millimeter wave thankst to the low number of RF chains. However, it remains an open question of the complexity of these schemes, especially given the wide bandwidth of millimeter wave communication.
Furthermore, there remains the need to achieve sufficient self-interference mitigation before receive chain components to prevent saturation.
