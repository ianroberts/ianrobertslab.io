---
layout: page
title: "Tackling Self-Interference with Beamforming in mmWave Systems"
summary: An introduction on how beamforming can be used mitigate self-interference.
date: 2021-11-25
published: true
---

> {{ page.summary }}


### Half-Duplexing Strategies versus Full-Duplex

For the past century or so, devices have typically operated in a half-duplex fashion by orthogonalizing transmission and reception in the time and/or frequency domains.
As illustrated below, this orthogonalizes transmission and reception by dedicating them a fraction of the time-frequency resources available to a system.

{% include image.html file="fdx/resource_tf_03_tdd.svg" caption="Time-division duplexing (TDD)." link=false %}

{% include image.html file="fdx/resource_tf_03_fdd.svg" caption="Frequency-division duplexing (FDD)." link=false %}

With full-duplex capability, a transceiver can offer all of the avaible time-frequency resources to both transmission and reception.
Naturally, this makes better use of the radio resources, allowing full-duplex transceivers to net a higher spectral efficiency than conventional half-duplex radios.
This also introduces unique opportunities for medium access and channel sensing, along with reduced latency since half-duplexing delays can be avoided, especially in time-division duplexing systems.

{% include image.html file="fdx/resource_tf_03_fdx.svg" caption="Full-duplexing." link=false %}


### Using the Spatial Domain to Mitigate Self-Interference

Millimeter wave systems make use of dense antenna arrays to overcome the severe path loss faced at such high carrier frequencies.
If we consider a full-duplex millimeter wave system where a dedicated array is used by the transmitter and another dedicated array is used by the receiver, thousands or tens of thousands of coupling paths of self-interference manifests between the transmitter and receiver.
While this complicates full-duplex operation in some ways, it also introduces a high-dimensional spatial domain as a promising means to tackle self-interference.

Rather than orthogonalize transmission and reception in the time-frequency plane, we instead aim to orthogonalize them in this high-dimensional spatial domain, as illustrated below.
Notice that transmission and reception make use of the full time-frequency resources, but are separated along the spatial axis.
This spatial orthogonality can be achieved by beamforming at the transmitter and/or receiver of the full-duplex device.

{% include image.html file="fdx/resource_tfs_type_04.svg" caption="" link=false %}

The figure above is somewhat misleading, however.
It is not our goal to orthogonalize transmission and reception, but rather self-interference and reception!
In fact, a multi-antenna transmit signal and a multi-antenna receive signal may not even be of the same dimensionality since the transmit array and receive array may have a different number of elements.
This motivates us to instead illustrate this concept as below.

{% include image.html file="fdx/resource_tfs_type_03.svg" caption="" link=false %}

Self-interference in multi-antenna transceivers is unique because, while the self-interference channel may not (significantly) shift the time-frequency components of a transmitted signal, it does transform its spatial components!
If transmit and receive beamforming can be tailored to strategically orthogonalize (or approximately orthogonalize) self-interference and a desired receive signal, full-duplex operation can be brought to the system.

Note that this concept has been explored in conventional sub-6 GHz radios.
However, the spatial mitigation of self-interfernece is particularly promising at millimeter wave because systems have a high-dimensional spatial domain and communicate very few data (spatial) streams.
This offers promise that there exists sufficient degrees of freedom to communicate these streams while mitigating self-interference.

[//]: # test

