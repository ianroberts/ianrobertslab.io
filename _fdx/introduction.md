---
layout: page
title: "Introduction to Full-Duplex"
summary: An introduction to in-band full-duplex.
categories: [wireless]
date: 2021-11-07
published: true
---



### The Problem

For the past century or so, wireless transceivers have typically operated in a half-duplex fashion, orthogonalizing transmission and reception in the time domain and/or the frequency domain.
This is to prevent _self-interference_, which manifests between the transmitter and receiver when a device attempts to receive while transmitting over the same spectrum.
Self-interference combines with a desired receive signal (i.e., the signal of interest) over the air before making its way through the receive chain of the full-duplex device.
Successful reception of a desired receive signal is often virtually impossible since self-interference is typically many orders of magnitude stronger than a desired receive signal (which has typically propagated a far distance).
This has motivated devices to operate in half-duplex fashion to circumvent self-interference.

{% include image.html file="fdx/fdx_sub6_siso_si.svg" width=70 caption="A transceiver operating in a full-duplex fashion subjects itself to self-interference." link=false %}


### The Solution

To successfully equip a transceiver with full-duplex capability, a means to mitigate self-interference is necessary.
The approach to mitigating self-interference is perhaps most naturally summarized by the following question.

> If a device knows what signal it transmitted, why can't it subtract that transmitted signal from the signal it receives to subtract self-interference and leave only the desired signal?

The simple fact that a full-duplex transceiver knows its own transmit signal offers it the exciting potential to cancel self-interference.
While knowing its transmit signal is powerful, it is not trivial to cancel self-interference since a _filtered version_ of the transmitted signal reaches the receiver, not the transmitted signal itself.
Put simply, self-interference is not a clean copy of the transmitted signal but rather a filtered one.
Nonetheless, a full-duplex transceiver can attempt to synthesize self-interference by estimating the self-interference channel that the transmit signal propogates through.
This synthesis of self-interference can be executed in the analog domain (i.e., in hardware) or digitally (i.e., in software/logic).

{% include image.html file="fdx/fdx_sub6_siso_asic.svg" width=70 caption="Analog self-interference cancellation aims to reconstruct and cancel self-interference in hardware." link=false %}

The figure above depicts analog self-interference cancellation, where a (physical) tunable filter is placed between the transmitter and receiver.
This filter is driven by a portion of the transmit signal and is injected at the receiver.
The full-duplex transceiver digitally estimates the self-interference channel during a quiet period, for instance, by transmitting a known signal and comparing it to the received signal.
The analog self-interference cancellation filter is then configured to replicate this estimate of the self-interference channel to synthesize self-interference that closely matches the true self-interference.
Then, the synthesized self-interference is inverted before injecting it at the receiver where it will cancel the true self-interference to leave the desired receive signal nearly interference free (if done properly).

{% include image.html file="fdx/fdx_sub6_siso_asic_dsic.svg" width=70 caption="A two-staged approach to self-interference cancellation, combining analog and digital stages." link=false %}

Self-interference cancellation can analogously be executed digitally, where filtering and subsequent injection with the received signal can take place in software/logic, which may be more sophisticated and flexible than analog self-interference cancellation.
There are practical factors that motivate the need for analog self-interference cancellation, meaning not all cancellation can take place digitally.
As such, most practical routes to full-duplex use a two-staged approach: first cancelling a portion of self-interference in analog, then the majority of residual self-interference digitally.


### Recommended Resources

Self-interference cancellation and full-duplex capability have been researched extensively since around 2010.
There are a number of sophisticated approaches to cancel self-interference, including and beyond digital and analog self-interference cancellation, many of which tackle artifacts of practical transceivers such as hardware imperfections.

Below is a list of recommended readings on a variety of topics related to full-duplex and self-interference cancellation.
- D. Bharadia et al., "Full Duplex Radios," 2013.
- D. Korpi et al., "Full-Duplex Transceiver System Calculations: Analysis of ADC and Linearity Challenges," 2014.


[//]: # test


