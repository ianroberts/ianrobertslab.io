---
layout: default
---

![The Engineering Education and Research Center (EER) at UT Austin](/images/ipr_eer.jpg#center, "The Engineering Education and Research Center (EER) at UT Austin")

I am a <del>first</del> <del>second</del> <del>third</del> fourth-year Ph.D. <del>student</del> candidate in the [electrical and computer engineering program](https://www.ece.utexas.edu) at the [University of Texas at Austin](https://utexas.edu).
I am fortunate to be supervised by [Prof. Jeff Andrews](https://sites.utexas.edu/jandrews/) and to be part of [his research group](https://sites.utexas.edu/jandrews/group/).
I am also advised by [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath) and part of his [Laboratory of Informatics, Networks, and Communications (LINC)](http://sriram.utlinc.org/#/utlinc).
Along with many others, we are members of the [Wireless Networking and Communications Group (WNCG)](https://wncg.org) and [6G@UT](http://6g-ut.org/).

I am fortunate to have [been selected](http://wncg.org/news/ian-roberts-wins-nsf-graduate-research-fellowship) as a [National Science Foundation](https://nsf.gov/) [Graduate Research Fellow](https://www.nsfgrfp.org/) beginning in Fall 2020. I appreciate their support of my research and studies.

I have created and released MIMO for MATLAB [(https://mimoformatlab.com)](https://mimoformatlab.com/), a package for simulating multiple-input multiple-output (MIMO) communication. It is completely [documented](https://mimoformatlab.com/docs/), and [video tutorials](https://www.youtube.com/playlist?list=PLQZnKEsUslUasrK1omM4gaN91d7N-0pZC) are in progress.

I was recently affiliated with [GenXComm](https://www.genxcomm.com/), a startup that develops a suite of full-duplex solutions that enable simultaneous transmission and reception in-band for various applications. GenXComm was founded in 2016 by [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath) and [Hardik B. Jain](https://www.linkedin.com/in/hardikbjain/).

I received my undergraduate electrical engineering degree from [Missouri University of Science and Technology](https://mst.edu) in May 2018. I was involved in undergraduate research supervised by [Prof. Y. Rosa Zheng](https://www.lehigh.edu/~yrz218/) on the topic of underwater acoustic communication.

I am a licensed amateur radio operator. My callsign is [KE0QVW](https://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=4013155).


## Research

My Ph.D. research aims to bring in-band full-duplex capability to millimeter wave communication systems by leveraging dense antenna arrays to mitigate self-interference in the spatial domain. 
I combine theory, measurements, and practical knowledge to motivate, develop, and validate my research in an effort to create full-duplex solutions that integrate into real millimeter wave communication systems.


## Interests

My broad research interests are in millimeter wave communication; in-band full-duplex; MIMO; communication system design, optimization, and simulation; interference cancellation; array signal processing; radar; and satellite communication systems.
My goal is to have extensive theoretical background on wireless combined with an understanding of how concepts are implemented practically. 
Once I am finished with school, I look forward to a career where I can research, develop, and prototype wireless technologies.


## Recent News

{% assign n = (site.data.news | sort: 'date') | reverse %}
{% for news in n limit:10 %}
- {{ news.date | date: "%B %Y" }}: {{ news.text }}  {% endfor %}


## Education

{% for s in site.data.schools %}

**{{ s.degree }}**, _{{ s.date }}_  
{{ s.school }}  

{% endfor %}


## Research Experience

{% for p in site.data.positions %}
{% if p.type == 'academic' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}


## Internship Experience

{% for p in site.data.positions %}
{% if p.type == 'internship' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}


## Recent Publications

{% assign pubs = (site.data.publications | sort: 'date' | reverse) %}
{% for p in pubs limit:3 %}
  {{ p.authors }}, "{{ p.title }}," _{{ p.journal }}_, {% if p.location %}{{ p.location }}, {% endif %}{{ p.date | date: "%b. %Y" }}. {% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}
{% endfor %}

[See all publications](/publications/) 

[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)


## Select Involvement & Service

- **Reviewer**, _IEEE Transactions on Communications_, _IEEE Access_, _IEEE Wireless Communications Magazine_, _IEEE Communications Letters_, _IEEE Open Journal of the Communications Society_, _IEEE International Conference on Communications_, _IEEE Global Communications Conference_, _IEEE International Symposium on Information Theory_, _IEEE Vehicular Technology Conference_.
- **Technical Program Committee Member**, [_IEEE ICC Workshop on Full-Duplex Communications for Future Wireless Networks_](https://icc2020.ieee-icc.org/workshop/ws-22-workshop-full-duplex-communications-future-wireless-networks), 2020.
- **Mentor**, [Graduates Linked with Undergraduates in Engineering](http://www.engr.utexas.edu/wep/mentoring/glue), [Women in Engineering Program](https://cockrell.utexas.edu/wep), Cockrell School of Engineering, University of Texas at Austin, 2019--2020.


## Contact

[my three initials] [at] utexas [dot] edu


## My Talented Friends

<ul>
{% for peer in site.data.peers %}
  <li><a href="{{ peer.site }}" target="_blank">{{ peer.name }}</a>, {{ peer.position }}</li>  
{% endfor %}
</ul>


{% for peer in site.data.peers %}
  [{{ peer.name | split: " " | first }}]: {{ peer.site }}  
{% endfor %}

[WNCG]: https://wncg.org
