---
layout: page
title: MIMO for MATLAB
categories: projects
permalink: mfm/
---

> A MATLAB package for simulating, researching, and learning sub-6 GHz and mmWave MIMO communication systems.

More information on MFM to come. Hopefully spring 2020 or summer 2020 I can mature the codebase and documentation. If you are interested in contributing to MFM, please reach out to me.


### Download MIMO for MATLAB (MFM)

<del>You can download MFM by downloading this zipped folder. Simply unzip it and it is ready to be used in MATLAB.</del>

MFM will be accessible again soon. Bug fixes are needed before it can be released again.


### What is MFM?

MFM is a suite of scripts, functions, and objects for conveniently implementing MIMO in MATLAB. MFM was designed with sub-6 GHz and millimeter-wave MIMO systems in mind. It has support for conventional half-duplex operation as well as full-duplex.

Within MFM are a suite of channel models, transceivers, and functions for implementing MIMO-related concepts.

Let's consider an example. Say you want to simulate a multi-user MIMO interference scenario where User A is transmitting to User B and interfering transmissions from User C are reaching User B. You may want to try out different MU-MIMO receiver strategies like zero-forcing (ZF) or MMSE approaches. With MFM, you can recreate this scenario and compare the performance of different receiver designs. If you come up with your own receiver design, you can also implement it if you like to see how it compares to the provided ZF and MMSE ones.

When learning MIMO for the first time, I found that using MATLAB to examine different scenarios numerically was a good way to obtain intuition.


### What isn't MIMO for MFM?

MFM is not a network simulator. It is only concerned with physical layer signaling and has no concept of medium access control, routing, data transmission, data reception, or the like. In its current state, MFM is only concerned with the metric of spectral efficiency based on channel realizations and the design of precoders and combiners. You are welcome to extend it as you'd like.


### Who is MFM for?

MFM was created as a tool that researchers can use to develop and verify their research.

In addition, it could be used in the classroom to demonstrate MIMO concepts to students.


### What will MFM become?

I hope MFM will be expanded to incorporate more modern research topics such as compressed sensing and machine learning. Additionally, I think I would like to expand its features to include nonlinear transceivers, channel estimation, and mmWave beamtraining.

Beyond its MIMO support, I want to improve its use as a tool for educators/researchers. For example, it would be convenient if MFM could automatically create equations, link budgets, and diagrams for the simulations being run.