---
layout: research
title: Research
categories: research
permalink: research/
public: false
hide_in_nav: true
---

> My Ph.D. reserach aims to bring in-band full-duplex capability to millimeter wave communication systems by combining theory, measurements, and practical knowledge.

[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)

#### Hybrid Beamforming for Full-Duplex mmWave Systems

#### Analog Beamforming Codebooks for Full-Duplex mmWave Systems

Like half-duplex mmWave
systems, one with full-duplex capability will presumably conduct codebook-based beam alignment on
its transmit link and receive link. Thus, it will juggle a transmit beam and receive beam concurrently,
which couple together via the self-interference channel. Off-the-shelf analog beamforming codebooks—
designed with half-duplex in mind—may be undesirable in full-duplex settings since they do not nec-
essarily offer robustness to self-interference. In this work, I develop a novel methodology for designing
analog beamforming codebooks for full-duplex mmWave transceivers, the first such codebooks to my
knowledge. My proposed design reduces the self-interference coupled by transmit-receive beam pairs
and simultaneously delivers high beamforming gain, allowing full-duplex mmWave systems to support
beam alignment while minimizing self-interference. To do so, my methodology allows some variability in
beamforming gain to strategically shape beams that reject self-interference while still having substantial
gain. Importantly, my design accounts for the non-convexity posed by digitally-controlled phase shifters
and attenuators. Codebooks based on my design can supplement other full-duplex solutions (such as
my hybrid beamforming design) and have the potential to enable full-duplex on their own.

#### Measurements of Self-Interference at 28 GHz using Phased Arrays

With: Aditya Chopra, Thomas Novlan.

#### Beam Selection for Full-Duplex mmWave Systems

#### Analysis of Full-Duplex Multi-Hop Networks

#### Analysis of Multi-Beam LEO Satellite Communication Systems

#### Test

#### Acknowledgment

Much of my work beginning Fall 2020 is supported by the National Science Foundation Graduate Research Fellowship Program (Grant No. DGE-1610403). Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation. I appreciate their support of my graduate research and studies.

#### Disclaimer

Personal use of this material is permitted. Permission from IEEE must be obtained for all other uses, in any current or future media, including reprinting/republishing this material for advertising or promotional purposes, creating new collective works, for resale or redistribution to servers or lists, or reuse of any copyrighted component of this work in other works.
