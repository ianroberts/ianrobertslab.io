## Projects

{% for project in site.projects reversed %}
**<a href="{{ project.url }}">{{ project.title }}</a>**, _{{ project.when }}_  
{{ project.summary }}

{% endfor %}