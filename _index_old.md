---
layout: default
---

![UT Austin](/images/tower.jpg#center, "UT Austin")

I am a <del>first</del> second-year Ph.D. student in the [electrical engineering program](https://www.ece.utexas.edu) at the [University of Texas at Austin](https://utexas.edu). My academic focus is on wireless communication, signal processing of communication systems, and in-band full-duplex. I am fortunate to be supervised by [Prof. Sriram Vishwanath](http://users.ece.utexas.edu/~sriram/) and to be part of the [Laboratory of Informatics, Networks, and Communications (LINC)](http://sriram.utlinc.org/#/utlinc) within the [Wireless Networking and Communications Group (WNCG)](https://wncg.org).

I received my undergraduate electrical engineering degree from [Missouri University of Science and Technology](https://mst.edu) in May 2018. During this time, I was involved in undergraduate research supervised by [Prof. Y. Rosa Zheng](https://www.lehigh.edu/~yrz218/) on the topic of underwater acoustic communication.

In addition to being a graduate student, I am affiliated with [GenXComm](https://www.genxcomm.com/), a local startup that develops a suite of solutions that enable simultaneous transmission and reception in-band for various applications.


## Interests

My interests are in wireless communication and signal processing of communication systems. Specifically, my interests and skills include PHY layer, MIMO, millimeter-wave, in-band full-duplex, optimization of MIMO communication systems, cellular systems, array signal processing, and communication system design and simulation. I enjoy learning new concepts and implementing them in MATLAB and Python. 

My short-term goals are to build my knowledge of wireless and to develop myself as an independent researcher. While doing so, it is my goal to mentor undergraduate students and introduce them to signal processing for communication systems. My long-term goal is to have extensive theoretical background combined with an understanding of how concepts are implemented practically. Once I am finished with school, I want a career where I can research, develop, and prototype wireless technologies.


## News

- February 2020: [Yitao](https://www.linkedin.com/in/yitao-chen-aa543b75/) defended!
- February 2020: The [MIMO for MATLAB](http://mimoformatlab.com) website went live, though it's still a work in progress.
- February 2020: I presented a poster at the [Graduate and Industry Networking](https://sites.utexas.edu/gain/) event hosted by the Cockrell School of Engineering. My friends (and fellow WNCG students) [Yuyang Wang](https://sites.google.com/site/yywangseu/) and [Marius Arvinte](https://www.linkedin.com/in/marius-arvinte) won poster awards!
- January 2020: I submitted a [workshop paper](https://arxiv.org/abs/2002.02127) to the [Workshop on Full-Duplex Communications for Future Wireless Networks](https://icc2020.ieee-icc.org/workshop/ws-22-workshop-full-duplex-communications-future-wireless-networks) at ICC 2020.
- December 2019: I presented my [conference paper](https://arxiv.org/abs/1908.06505) at GLOBECOM 2019 in Waikoloa, Hawaii. It was a fun trip with my friends (and fellow WNCG students) [Ahmad AlAmmouri](https://alammouri.com/), [Marius Arvinte](https://www.linkedin.com/in/marius-arvinte), and [Yunseong Cho](https://sites.google.com/utexas.edu/yscho).
- November 2019: I submitted a [conference paper](https://arxiv.org/abs/1911.11283) to the 2020 IEEE International Radar Conference.
- November 2019: I attended the [Texas Wireless Summit](https://www.texaswirelesssummit.org/) hosted by [WNCG](http://wncg.org/).
- October 2019: I submitted a [conference paper](https://arxiv.org/abs/1910.11983) to ICC 2020.
- August 2019: I started [MIMO for MATLAB](http://mimoformatlab.com).
- Summer 2019: I interned with Amazon's Wireless Technologies Group in Sunnyvale, CA.
- April 2019: I submitted a [conference paper](https://arxiv.org/abs/1908.06505) to GLOBECOM 2019. 


## Education

{% for s in site.data.schools %}

**{{ s.school }}**, _{{ s.date }}_  
{{ s.degree }}  

{% endfor %}


## Experience

{% for p in site.data.positions %}

**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  

{% endfor %}


## Publications

**I. P. Roberts** and S. Vishwanath, "Beamforming cancellation design for millimeter-wave full-duplex", _Proceedings of the IEEE Global Communications Conference: Signal Processing for Communications_, Waikoloa, HI, USA, Dec. 2019. [[arXiv](https://arxiv.org/abs/1908.06505)] [[IEEE](https://ieeexplore.ieee.org/document/9013116)].

**I. P. Roberts**, H. B. Jain, and S. Vishwanath, "Frequency-selective beamforming cancellation design for millimeter-wave full-duplex", _Proceedings of the IEEE International Conference on Communications_, Dublin, Ireland, June 2020. [[arXiv](https://arxiv.org/abs/1910.11983)].

H. B. Jain, **I. P. Roberts**, and S. Vishwanath, "Enabling in-band coexistence of millimeter-wave communication and radar", _Proceedings of the IEEE International Radar Conference_, Washington, D.C., USA, Apr. 2020. [[arXiv](https://arxiv.org/abs/1911.11283)].

**I. P. Roberts**, H. B. Jain, and S. Vishwanath, "Equipping millimeter-wave full-duplex with analog self-interference cancellation", _Proceedings of the IEEE International Conference on Communications Workshop on Full-Duplex Communications for Future Wireless Networks_, Dublin, Ireland, June 2020. [[arXiv](https://arxiv.org/abs/2002.02127)].


## Graduate Coursework

{% for course in site.data.courses %}

**{{ course.name }}**, _{{ course.prof }}_  
{{ course.short }}  

{% endfor %}


## Recent Involvement

- **Technical Program Committee Member**, [Workshop on Full-Duplex Communications for Future Wireless Networks](https://icc2020.ieee-icc.org/workshop/ws-22-workshop-full-duplex-communications-future-wireless-networks), IEEE International Conference on Communications (ICC), June 2020.
- **Mentor**, [Graduates Linked with Undergraduates in Engineering](http://www.engr.utexas.edu/wep/mentoring/glue), Women in Engineering Program, Cockrell School of Engineering, University of Texas at Austin, Spring 2020.
- **Session Chair**, Massive MIMO III, IEEE Global Communications Conference (GLOBECOM), December 2019.
- **Mentor**, [Graduates Linked with Undergraduates in Engineering](http://www.engr.utexas.edu/wep/mentoring/glue), Women in Engineering Program, Cockrell School of Engineering, University of Texas at Austin, Fall 2019.


## Contact

ipr [at] utexas [dot] edu

[LinkedIn](https://www.linkedin.com/in/iproberts/)

EER 6.836  
2501 Speedway  
Austin, TX 78712


## My Talented Friends

<ul>
{% for peer in site.data.peers %}
  <li><a href="{{ peer.site }}" target="_blank">{{ peer.name }}</a>, {{ peer.position }}</li>  
{% endfor %}
</ul>
