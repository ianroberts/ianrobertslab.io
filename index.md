---
layout: home
bio-image: /images/ipr-eer.jpg
bio-tag: Hello, I'm Ian Roberts.
bio-line-1: >
  I am an Assistant Professor at <a href="https://ee.ucla.edu" target="_blank">UCLA</a> working on wireless communication and sensing. All roads lead to Westwood...
bio-line-2: >
  I direct the <a href="https://wireless.ee.ucla.edu">Wireless Lab</a> within UCLA's Department of Electrical and Computer Engineering.
bio-line-: >
  I recently completed my Ph.D. at <a href="https://www.ece.utexas.edu" target="_blank">UT Austin</a> in the <a href="https://wncg.org" target="_blank">Wireless Networking and Communications Group</a>.
  
---

<p style="margin-top: 0px;"><hr style="height:1.5px; border:0; color:#828282; background-color:#828282; margin-top: 0px;"></p>

I am an Assistant Professor at [UCLA](https://ucla.edu) in the [Department of Electrical and Computer Engineering](https://www.ee.ucla.edu) within the [Samueli School of Engineering](https://samueli.ucla.edu/).

I completed my Ph.D. in [electrical and computer engineering](https://www.ece.utexas.edu) at the [University of Texas at Austin](https://utexas.edu), where I was fortunate to be supervised by [Prof. Jeff Andrews](https://sites.utexas.edu/jandrews/) and [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath).
During my Ph.D., I am grateful to have been an [NSF Graduate Research Fellow](https://www.nsfgrfp.org/) in the [Wireless Networking and Communications Group (WNCG)](https://wncg.org) and the [6G@UT Research Center](https://6g-ut.org/).

<!---
I am honored to have [recently been awarded](https://www.ieeefoundation.org/ian-roberts-receives-andrea-goldsmith-young-scholars-award/) the [2023 Andrea Goldsmith Young Scholars Award](https://comt.committees.comsoc.org/awards/andrea-goldsmith-young-scholars-award/) by the [Communication Theory Technical Committee](https://comt.committees.comsoc.org) of the IEEE Communications Society.
--->

Much of my research aims to upgrade next-generation wireless systems, like 5G and future 6G cellular systems, with full-duplex capability: the long-sought ability to simultaneously transmit and receive signals over the same frequency spectrum.
I strive to combine both theory and experimentation to motivate, develop, and validate my research, with the hope of having impact on real-world wireless systems.

The GitHub repositories associated with several of my papers are listed below:
- [Beamformed self-interference measurements at 28 GHz](https://github.com/iproberts/beamformed_si_measurements)
- [Spatial and statistical modeling of mmWave self-interference](https://github.com/iproberts/bfsi_model)
- [STEER: Beam selection for full-duplex mmWave communication systems](https://github.com/iproberts/steer)
- [LoneSTAR: Beam codebooks for full-duplex mmWave communication systems](https://github.com/iproberts/lonestar)

I have collaborated extensively with [AT&T Labs](https://about.att.com/sites/labs) to characterize and enable real-world full-duplex millimeter wave communication systems.
I have also collaborated with [Prof. Ahmed Alkhateeb](https://scholar.google.com/citations?user=dLHw2qcAAAAJ) at Arizona State University and [Prof. Chan-Byoung Chae](https://scholar.google.com/citations?hl=en&user=A-NGz3MAAAAJ) at Yonsei University.
In 2023, I co-authored a [book chapter](/pdf/pub/chapter.pdf) on full-duplex for next-generation wireless systems with [Prof. Himal Suraweera](https://scholar.google.com/citations?user=r_QyMNwAAAAJ&hl=en).

During 2018--2020, I was heavily involved in early research and development at [GenXComm (GXC)](https://www.gxc.io/), a startup that creates a suite of full-duplex solutions that enable simultaneous transmission and reception in-band for various applications. GenXComm was co-founded by my Ph.D. co-advisor [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath).

Beyond this, I also have industry experience developing and prototyping wireless technologies at AT&T Labs, Amazon, Sandia National Labs, and Dynetics, Inc.

I received my undergraduate electrical engineering degree from [Missouri University of Science and Technology](https://mst.edu), during which I was involved in undergraduate research supervised by [Prof. Y. Rosa Zheng](https://www.lehigh.edu/~yrz218/) on the topic of underwater acoustic communication.

I am a licensed amateur radio operator. My callsign is [KE0QVW](https://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=4013155).

<!---
## Recent News

{% assign n = (site.data.news | sort: 'date') | reverse %}
{% for news in n limit:10 %}
- {{ news.date | date: "%B %Y" }}: {{ news.text }}  {% endfor %}
--->

## IEEE-Style Biography

Ian P. Roberts is an Assistant Professor at UCLA in the Department of Electrical and Computer Engineering. 
He received the B.S. degree in electrical engineering from Missouri University of Science and Technology and the M.S. and Ph.D. degrees in electrical and computer engineering from the University of Texas at Austin, where he was a National Science Foundation Graduate Research Fellow with the Wireless Networking and Communications Group. 
He has industry experience developing and prototyping wireless technologies at AT&T Labs, Amazon, GenXComm (startup), and Sandia National Labs. 
His research interests are in the theory and implementation of millimeter wave systems, full-duplex, and other next-generation technologies for wireless communication and sensing. 
In 2023, he received the Andrea Goldsmith Young Scholars Award from the Communication Theory Technical Committee of the IEEE Communications Society.


## Education

{% for s in site.data.schools %}

**{{ s.degree }}**, _{{ s.date }}_  
{{ s.school }}  

{% endfor %}

<!---
## Academic Research Experience

{% for p in site.data.positions %}
{% if p.type == 'academic' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}
--->


## Industry Experience

{% for p in site.data.positions %}
{% if p.type == 'internship' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}


<!---
## Three Recent Publications

{% assign pubs = (site.data.publications | sort: 'date' | reverse) %}
{% for p in pubs limit:3 %}
  {{ p.authors }}, "{{ p.title }}," _{{ p.journal }}_, {% if p.location %}{{ p.location }}, {% endif %}{{ p.date | date: "%b. %Y" }}. {% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}
{% endfor %}

[See all publications](/publications/) 

[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)



## Contact

ianroberts [at] ucla [dot] edu
--->

## My Talented Friends

<ul>
{% for peer in site.data.peers %}
  <li><a href="{{ peer.site }}" target="_blank">{{ peer.name }}</a>, {{ peer.position }}</li>  
{% endfor %}
</ul>

<!---
## A Random Picture Related to Wireless

<p style="text-align: center">
<button onclick="choosePic();">Click for Another Random Picture</button> 
</p>

<p style="text-align: center">
<img src="images/random/0.jpg" id="randPicture" />
</p>
--->


{% for peer in site.data.peers %}
  [{{ peer.name | split: " " | first }}]: {{ peer.site }}  
{% endfor %}

[WNCG]: https://wncg.org
