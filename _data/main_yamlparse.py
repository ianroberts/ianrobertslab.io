#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 10:36:32 2019

@author: ian
"""

import yaml

preamble = 'cv/preamble.tex'
cv = 'cv/cv.tex'

f = open(preamble,'r')
if f.mode == 'r':
  pre = f.read()

print(pre)

filename = 'schools.yaml'
with open(filename, 'r') as stream:
    try:
        y = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        
s = pre

s = s + '\\begin{document}'

s = s + '\\begin{rSection}{Education}'

for i in range(len(y)):
  who = y[i]['school']
  when = y[i]['date']
  what = y[i]['degree']
  
  s = s + '\\begin{rSubsectionEd}'
  s = s + '{' + who + '}{' + when + '}{' + what + '}'
  s = s + '\end{rSubsectionEd}'

s = s + '\end{rSection}'

print(s)

filename = 'positions.yaml'
with open(filename, 'r') as stream:
    try:
        y = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        
s = s + '\\begin{rSection}{Experience}'

for i in range(len(y)):
  who = y[i]['company']
  when = y[i]['date']
  what = y[i]['position']
  where = y[i]['location']
  
  s = s + '\\begin{rSubsection2}'
  s = s + '{' + who + '}{' + when + '}{' + what + '}{' + where + '}'
  s = s + '\end{rSubsection2}'

s = s + '\end{rSection}'

print(s)

filename = 'courses.yaml'
with open(filename, 'r') as stream:
    try:
        y = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

s = s + '\\begin{rSection}{Graduate Coursework}'

for i in range(len(y)):
  course = y[i]['name']
  prof = y[i]['prof']
  summary = y[i]['summary']
  
  s = s + '\\begin{rSubsectionCourse}'
  s = s + '{' + course + '}{' + prof + '}{' + summary + '}'
  s = s + '\end{rSubsectionCourse}'

s = s + '\end{rSection}'

print(s)

s = s + '\end{document}'

s = s.replace('&','\&')

print(s)

f = open(cv,'w+')
f.write(s)
f.close()