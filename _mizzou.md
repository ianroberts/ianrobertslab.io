---
layout: page
title: University of Missouri
categories: mizzou
permalink: mizzou/
hide_in_nav: true
---

> Please see below for materials related to my on-site interview with the University of Missouri.

[Faculty Talk Slides](/pdf/mizzou/slides.pdf)

[Full CV](/pdf/mizzou/cv.pdf)

[MIMO for MATLAB (a software package)](https://mimoformatlab.com)

{% assign pubs = (site.data.publications | sort: 'date') | reverse %}

Publications (Preprint, Journal, Magazine, and Conference):
{% for p in pubs %}
{% if p.pdf %} - [{{ p.title }}]({{ p.pdf }}){% endif %}
{% endfor %} 

There are some preprints whose PDFs are available upon request (not listed here but in my CV). They cannot be released publicly due to limited release restrictions imposed by industry collaborators until the paper's acceptance.
