---
layout: page
title: Resources
categories: resources
permalink: resources/
---

> Here are a collection of resources I have found helpful for various topics.


### Graduate School

{% for b in site.data.resources.gradschool %}

<a href="{{ b.link }}" target="_blank">{{ b.title }}</a>
{% endfor %}


### Technical Writing

{% for b in site.data.resources.writing %}

<a href="{{ b.link }}" target="_blank">{{ b.title }}</a>
{% endfor %}


### LaTeX

