---
layout: page
title: Photos
hide_in_nav: true
categories: photos
permalink: photos/
---

> Some photos I'd like to share! Most photos were taken on my smartphone.  

{% assign photos = (site.data.photos | sort: 'date') | reverse %}
{% for p in photos %}
  {% assign f = "photos/" | append: p.file %}
  {% assign cap = p.caption %}
  {% assign mydate = p.date | date: "%B %Y" %}
  {% assign d = mydate | | append: ") " | prepend: "(" %}
  {% assign c = cap | prepend: d %}
  {% include image.html file=f caption=c link=true %}<br>  
{% endfor %}
