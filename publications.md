---
layout: page
title: Publications
categories: publications
permalink: publications/
---


[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)

{% assign pubs = (site.data.publications | sort: 'date') | reverse %}


### Preprint

{% for p in pubs %}
{% if p.type == 'preprint' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.poster %}[**[poster]**]({{ p.poster }}) {% endif %}{% if p.bibtex %}[**[BibTeX]**]({{ p.bibtex }}) {% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>){% endif %}  
<br>
{% endif %}
{% endfor %}


### Journal and Magazine

{% for p in pubs %}
{% if p.type == 'journal' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%b %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.poster %}[**[poster]**]({{ p.poster }}) {% endif %}{% if p.bibtex %}[**[BibTeX]**]({{ p.bibtex }}) {% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>){% endif %}  
<br>
{% endif %}
{% endfor %}


### Book Chapter

{% for p in pubs %}
{% if p.type == 'book' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%b %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>){% endif %}  
<br>
{% endif %}
{% endfor %}


### Conference

{% for p in pubs %}
{% if p.type == 'conference' %}
**{{ p.title }}**  
by {{ p.authors }}  
{% if p.journal %}_{{ p.journal }}_, {% endif %}{{ p.date | date: "%b %Y" }}  
{% if p.short %}_<span style="color: gray">{{ p.short }}</span><br>_{% endif %}{% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}{% if p.bibtex %}[**[BibTeX]**]({{ p.bibtex }}) {% endif %}{% if p.note %}(<span style="color: red">{{ p.note }}</span>){% endif %}  
<br>
{% endif %}
{% endfor %}


### Acknowledgment

Much of my work in 2020--2023 is supported by the National Science Foundation Graduate Research Fellowship Program (Grant No. 1610403). Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation. I appreciate their support of my graduate research and studies.

### Disclaimer

Personal use of this material is permitted. Permission from respective copyright holders (e.g., IEEE, Springer) must be obtained for all other uses, in any current or future media, including reprinting/republishing this material for advertising or promotional purposes, creating new collective works, for resale or redistribution to servers or lists, or reuse of any copyrighted component of this work in other works.
