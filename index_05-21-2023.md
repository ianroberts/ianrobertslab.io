---
layout: home
bio-image: /images/ipr-eer.jpg
bio-tag: Hello, I'm Ian Roberts.
bio-summary: >
  I am a fifth-year Ph.D. candidate at the <a href="https://utexas.edu" target="_blank">University of Texas at Austin</a>. My research is in wireless communication.
---

I am a <del>first</del> <del>second</del> <del>third</del> <del>fourth</del> fifth-year Ph.D. <del>student</del> candidate in the [electrical and computer engineering program](https://www.ece.utexas.edu) at the [University of Texas at Austin](https://utexas.edu).
I am fortunate to be supervised by [Prof. Jeff Andrews](https://sites.utexas.edu/jandrews/) and to be part of [his research group](https://sites.utexas.edu/jandrews/group/).
I am also advised by [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath) and part of his [Laboratory of Informatics, Networks, and Communications (LINC)](http://sriram.utlinc.org/#/utlinc).
Along with many others, we are members of the [Wireless Networking and Communications Group (WNCG)](https://wncg.org) and the [6G@UT Research Center](https://6g-ut.org/).

I am honored to have [recently been awarded](https://www.ieeefoundation.org/ian-roberts-receives-andrea-goldsmith-young-scholars-award/) the [2023 Andrea Goldsmith Young Scholars Award](https://comt.committees.comsoc.org/awards/andrea-goldsmith-young-scholars-award/) by the [Communication Theory Technical Committee](https://comt.committees.comsoc.org) of the IEEE Communications Society.

I am fortunate to have [been selected](http://wncg.org/news/ian-roberts-wins-nsf-graduate-research-fellowship) as a [National Science Foundation](https://nsf.gov/) [Graduate Research Fellow](https://www.nsfgrfp.org/). I appreciate their support of my research and studies.

The GitHub repositories associated with several of my papers are listed below:
- [Beamformed self-interference measurements at 28 GHz](https://github.com/iproberts/beamformed_si_measurements)
- [Spatial and statistical modeling of mmWave self-interference](https://github.com/iproberts/bfsi_model)
- [STEER: beam selection for full-duplex mmWave communication systems](https://github.com/iproberts/steer)
- [LoneSTAR: beam codebooks for full-duplex mmWave communication systems](https://github.com/iproberts/lonestar)

I have created and released MIMO for MATLAB [(https://mimoformatlab.com)](https://mimoformatlab.com/), a publicly available package for simulating multiple-input multiple-output (MIMO) communication. It is completely [documented](https://mimoformatlab.com/docs/), and [video tutorials](https://www.youtube.com/playlist?list=PLQZnKEsUslUasrK1omM4gaN91d7N-0pZC) are in progress.

I was recently affiliated with [GenXComm (GXC)](https://www.gxc.io/), a startup that develops a suite of full-duplex solutions that enable simultaneous transmission and reception in-band for various applications. GenXComm was founded in 2016 by [Prof. Sriram Vishwanath](http://wncg.org/people/faculty/sriram-vishwanath) and [Hardik B. Jain](https://ieeexplore.ieee.org/author/37087043651).

I received my undergraduate electrical engineering degree from [Missouri University of Science and Technology](https://mst.edu) in May 2018. I was involved in undergraduate research supervised by [Prof. Y. Rosa Zheng](https://www.lehigh.edu/~yrz218/) on the topic of underwater acoustic communication.

I am a licensed amateur radio operator. My callsign is [KE0QVW](https://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=4013155).


## IEEE-Style Biography

Ian P. Roberts received the B.S. degree in electrical engineering from the Missouri University of Science and Technology and the M.S. degree in electrical and computer engineering from the University of Texas at Austin, where he is currently a Ph.D. candidate and part of the 6G@UT Research Center and the Wireless Networking and Communications Group. 
He has industry experience developing and prototyping wireless technologies at AT&T Labs, Amazon, GenXComm (startup), Sandia National Laboratories, and Dynetics, Inc.
His research interests are in the theory and implementation of millimeter wave systems, in-band full-duplex, and other next-generation technologies for wireless communication and sensing.
He is a National Science Foundation Graduate Research Fellow.


## Ph.D. Research

My Ph.D. research aims to bring in-band full-duplex capability to millimeter wave communication systems by leveraging dense antenna arrays to mitigate self-interference in the spatial domain. 
I combine theory, practical knowledge, and measurements to motivate, develop, and validate my research in an effort to create full-duplex solutions that integrate into real millimeter wave communication systems.


## Research Interests

My broad research interestes are in wireless communication and sensing; communication system design, optimization, and simulation; millimeter wave; full-duplex; satellite communication systems; array signal processing; radar; measurements and prototyping.
I aim to develop technologies that advance next-generation wireless communication systems by combining theory, practical knowledge, measurements, and experimental proofs-of-concept.
Once I am finished with school, I look forward to a career where I can continue to research, develop, and prototype wireless technologies.



## Recent News

{% assign n = (site.data.news | sort: 'date') | reverse %}
{% for news in n limit:10 %}
- {{ news.date | date: "%B %Y" }}: {{ news.text }}  {% endfor %}


## Education

{% for s in site.data.schools %}

**{{ s.degree }}**, _{{ s.date }}_  
{{ s.school }}  

{% endfor %}


## Research Experience

{% for p in site.data.positions %}
{% if p.type == 'academic' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}


## Industry Experience

{% for p in site.data.positions %}
{% if p.type == 'internship' %}
**{{ p.company }}**, _{{ p.date }}_  
{{ p.position }}  
{% endif %}
{% endfor %}


## Recent Publications

{% assign pubs = (site.data.publications | sort: 'date' | reverse) %}
{% for p in pubs limit:3 %}
  {{ p.authors }}, "{{ p.title }}," _{{ p.journal }}_, {% if p.location %}{{ p.location }}, {% endif %}{{ p.date | date: "%b. %Y" }}. {% if p.pdf %}[**[PDF]**]({{ p.pdf }}) {% endif %}{% if p.ieee %}[**[IEEE]**]({{ p.ieee }}) {% endif %}{% if p.arxiv %}[**[arXiv]**]({{ p.arxiv }}) {% endif %}{% if p.code %}[**[code]**]({{ p.code }}) {% endif %}{% if p.youtube %}[**[YouTube]**]({{ p.youtube }}) {% endif %}{% if p.slides %}[**[slides]**]({{ p.slides }}) {% endif %}
{% endfor %}

[See all publications](/publications/) 

[Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)


## Select Involvement & Service

- **Member**, [_Full-Duplex and Self-Interference Cancellation Emerging Technologies Initiative_](https://fdc.committees.comsoc.org/), IEEE Communications Society.
- **Technical Program Committee Member**, [_IEEE ICC Workshop on Full-Duplex Communications for Future Wireless Networks_](https://icc2020.ieee-icc.org/workshop/ws-22-workshop-full-duplex-communications-future-wireless-networks), 2020.
- **Mentor**, Graduates Linked with Undergraduates in Engineering, [Women in Engineering Program](https://cockrell.utexas.edu/wep), Cockrell School of Engineering, University of Texas at Austin, 2019--2020.
- **Reviewer**, _IEEE Transactions on Wireless Communications_, _IEEE Transactions on Communications_, _IEEE Access_, _IEEE Wireless Communications Magazine_, _IEEE Communications Letters_, _IEEE Open Journal of the Communications Society_, _IEEE International Conference on Communications_, _IEEE Global Communications Conference_, _IEEE International Symposium on Information Theory_, _IEEE Information Theory Workshop_, _IEEE Vehicular Technology Conference_, _IEEE International Symposium on Personal, Indoor and Mobile Radio Communications_.


## Contact

[my three initials] [at] utexas [dot] edu


## My Talented Friends

<ul>
{% for peer in site.data.peers %}
  <li><a href="{{ peer.site }}" target="_blank">{{ peer.name }}</a>, {{ peer.position }}</li>  
{% endfor %}
</ul>


## A Random Picture Related to Wireless

<p style="text-align: center">
<button onclick="choosePic();">Click for Another Random Picture</button> 
</p>

<p style="text-align: center">
<img src="images/random/0.jpg" id="randPicture" />
</p>


{% for peer in site.data.peers %}
  [{{ peer.name | split: " " | first }}]: {{ peer.site }}  
{% endfor %}

[WNCG]: https://wncg.org
