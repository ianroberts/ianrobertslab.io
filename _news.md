## News

- January 2020: I submitted a workshop paper to the Workshop on Full-Duplex Communications for Future Wireless Networks at ICC 2020.
- January 2020: I posted a few new [notes](/notes/).
- December 2019: I presented my conference paper at GLOBECOM 2019 in Waikoloa, Hawaii, USA.
- November 2019: I submitted a conference paper to the 2020 IEEE International Radar Conference. [[arXiv](https://arxiv.org/abs/1911.11283)]
- November 2019: I attended the [Texas Wireless Summit](https://www.texaswirelesssummit.org/) hosted by [WNCG](http://wncg.org/).
- October 2019: I submitted a conference paper to ICC 2020. [[arXiv](https://arxiv.org/abs/1910.11983)]
- Summer 2019: I interned with Amazon's Wireless Technologies Group in Sunnyvale, CA.
- April 2019: I submitted a conference paper to GLOBECOM 2019. [[arXiv](https://arxiv.org/abs/1908.06505)]