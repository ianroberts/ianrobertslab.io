---
layout: note
title:  "Short Course on Wireless"
categories: [wireless]
date: 2020-01-25
summary: I have put together a short course on wireless and have made the slides available here. I hope you find them useful in introducing you to wireless or providing you a refresher on the topics covered.
---

Below are slides I have created as a means for introducing a newcomer to the world of wireless communication. 

Ideally, the slides would be viewed in the order as listed. The order may seem somewhat strange; this is the route I have found to ease the introduction of wireless while providing some perspective along the way. For example, students can learn about the frequency domain using their knowledge of FM radio stations before truly understanding the time-frequency relationship.

The topics covered are not meant to be a complete coverage of all wireless topics. I intentionally chose to include only the material within according the original purpose for which these slides were created. Most of what I present is a high-level introduction rather than a mathematical description of wireless. This is meant to be a soft introduction to the topic of wireless, free from wading through detailed math. My goal was to create slides that have visual aids for grasping what would otherwise be somewhat abstract to a newcomer.

- Lecture 1: Digital Information, [[pdf](/pdf/shortcoursewireless/01_digital_information.pdf)]
- Lecture 2: Sine Waves, [[pdf](/pdf/shortcoursewireless/02_sine_waves.pdf)]
- Lecture 3: What is a Radio?, [[pdf](/pdf/shortcoursewireless/03_what_is_a_radio.pdf)]
- Lecture 4: Antennas, [[pdf](/pdf/shortcoursewireless/04_antennas.pdf)]

Below are some upcoming lectures that I hope to get to soon.

- Lecture 5: From the Frequency Perspective, coming soon
- Lecture 6: Filtering, coming soon
- Lecture 7: Time-Frequency Relationship, coming soon
- Lecture 8: Upconversion and Downconversion, coming soon
- Lecture 9: RF Systems, coming soon
- Lecture 10: Wireless Channels, coming soon
- Lecture 11: Sampling Theory, coming soon

I make no guarantee on the accuracy of this material. Hopefully there aren't any mistakes or typos.
