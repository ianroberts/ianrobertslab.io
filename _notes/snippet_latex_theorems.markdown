---
layout: note
title:  "Code Snippet: Theorems, Etc. in LaTeX"
date: 2020-11-16
categories: [latex]
summary: A code snippet for theorems, corollaries, lemmas, definitions, and remarks in LaTeX.
---

### Setup

Put these two things in your preamble.

We use the `amsthm` package.

{% highlight latex %}
\usepackage{amsthm}
{% endhighlight %}

The following sets up the styling and formatting of the environments.

{% highlight latex %}
% To create a custom style
% \newtheoremstyle{customname}{3pt}{3pt}{bodyfont}{indentamt}{headfont}{headpunct}{headspace}{headspec}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}

\theoremstyle{plain}
\newtheorem{corollary}{Corollary}[theorem]

\theoremstyle{plain}
\newtheorem{lemma}[theorem]{Lemma}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

% To change the QED symbol to filled.
% \renewcommand\qedsymbol{$\blacksquare$}
{% endhighlight %}

### Usage

{% highlight latex %}
\begin{theorem}
Theorem here...
\begin{proof}
Proof of theorem here...
\end{proof}
\end{theorem}
{% endhighlight %}


{% highlight latex %}
\begin{corollary}
Corollary here...
\end{corollary}
{% endhighlight %}


{% highlight latex %}
\begin{remark}
Remark here...
\end{remark}
{% endhighlight %}


{% highlight latex %}
\begin{lemma}
Lemma here...
\end{lemma}
{% endhighlight %}


{% highlight latex %}
\begin{definition}[Optional title.]
Definition here...
\end{definition}
{% endhighlight %}


