---
layout: note
title:  "Creating a Glossary of Acronyms in LaTeX"
date: 2020-11-20
categories: [latex]
summary: A brief introduction to creating a glossary of acronyms in LaTeX.
---

### Setup

I use the `glossaries` package with the following settings, which aren't all important unless printing the glossary terms.

{% highlight latex %}
\usepackage[acronym,nogroupskip,nonumberlist,nopostdot]{glossaries}
\makeglossaries
{% endhighlight %}


### Defining Acronyms

Acronyms are defined in the following fashion

{% highlight latex %}
\newacronym{<key>}{<acronym>}{<full term>}
{% endhighlight %}

For example, signal-to-noise ratio (SNR) would be defined via

{% highlight latex %}
\newacronym{snr}{SNR}{signal-to-noise ratio}
{% endhighlight %}


### Usage

To use the SNR acronym, simply type `\gls{snr}` anywhere you want to use it.

The first time the acronym is used, the acronym will be defined. Thereafter, it will be referred to by its acronym only.

For example, the following LaTeX

{% highlight latex %}
The \gls{snr} is very low. The \gls{snr} should be higher.
{% endhighlight %}

yields

> The signal-to-noise ratio (SNR) is very low. The SNR should be higher.

### Shorthand Macros

Sometimes it is cumbersome to type `\gls{snr}` anytime I want to call the SNR acronym. It would be more convenient to type `\snr` instead, so I create shorthand macros for the acronyms I use most.

The `xspace` package is useful when creating glossary shorthand macros to automatically insert/neglect spaces when appropriate.

{% highlight latex %}
\usepackage{xspace}
{% endhighlight %}

A shorthand macro can then be defined via

{% highlight latex %}
\newcommand{\snr}{\gls{snr}\xspace}
{% endhighlight %}

The following LaTeX

{% highlight latex %}
The \snr is very low. The \snr should be higher.
{% endhighlight %}

yields

> The signal-to-noise ratio (SNR) is very low. The SNR should be higher.


### Some Useful Acronyms

{% highlight latex %}
\newacronym{snr}{SNR}{signal-to-noise ratio}
\newacronym{sinr}{SINR}{signal-to-interference-plus-noise ratio}
\newacronym{inr}{INR}{interference-to-noise ratio}
\newacronym{sir}{SIR}{signal-to-interference ratio}
\newacronym{sqr}{SQR}{signal-to-quantization-noise ratio}
\newacronym{sqnr}{SQNR}{signal-to-quantization-plus-noise ratio}

\newacronym{ber}{BER}{bit error rate}
\newacronym{evm}{EVM}{error vector magnitude}
\newacronym{isi}{ISI}{intersymbol interference}

\newacronym{bfsk}{BFSK}{binary frequency shift keying}
\newacronym{qam}{QAM}{quadrature amplitude modulation}
\newacronym{mqam}{MQAM}{M-ary quadrature amplitude modulation}
\newacronym{dsss}{DSSS}{direct-sequence spread spectrum}
\newacronym{ofdm}{OFDM}{orthogonal frequency-division multiplexing}
\newacronym{ofdma}{OFDMA}{orthogonal frequency-division multiple access}
\newacronym{fdd}{FDD}{frequency-division duplexing}
\newacronym{tdd}{TDD}{time-division duplexing}
\newacronym{fdma}{FDMA}{frequency-division multiple access}
\newacronym{tdma}{TDMA}{time-division multiple access}
\newacronym{sdma}{SDMA}{space-division multiple access}

\newacronym{ls}{LS}{least-squares}
\newacronym{lms}{LMS}{least mean squares}
\newacronym{omp}{OMP}{orthogonal matching pursuit}

\newacronym{zf}{ZF}{zero-forcing}
\newacronym{mmse}{MMSE}{minimum mean square error}
\newacronym{mse}{MSE}{mean square error}

\newacronym{fft}{FFT}{fast Fourier transform}
\newacronym{dft}{DFT}{discrete Fourier transform}
\newacronym{dtft}{DTFT}{discrete-time Fourier transform}
\newacronym{ctft}{CTFT}{continuous-time Fourier transform}

\newacronym{adc}{ADC}{analog-to-digital converter}
\newacronym{dac}{DAC}{digital-to-analog converter}
\newacronym{fpga}{FPGA}{field-programmable gate array}
\newacronym{enob}{ENOB}{effective number of bits}

\newacronym{rv}{r.v.}{random variable}
\newacronym{svd}{SVD}{singular value decomposition}
\newacronym{sdp}{SDP}{semidefinite programming}
\newacronym{psd}{PSD}{positive semidefinite}
\newacronym{nsd}{NSD}{negative semidefinite}

\newacronym{agc}{AGC}{automatic gain control}
\newacronym{rf}{RF}{radio frequency}
\newacronym{los}{LOS}{line-of-sight}
\newacronym{nlos}{NLOS}{non-line-of-sight}
\newacronym{ple}{PLE}{path loss exponent}
\newacronym[plural=dB,firstplural=decibels (dB)]{db}{dB}{decibel}
\newacronym[plural=dBm,firstplural=decibel milliwatts (dBm)]{dbm}{dBm}{decibel milliwatts}
\newacronym{pa}{PA}{power amplifier}
\newacronym{lna}{LNA}{low noise amplifier}
\newacronym{cw}{CW}{continuous wave}
\newacronym{papr}{PAPR}{peak-to-average power ratio}
\newacronym{tx}{TX}{transmitter}
\newacronym{rx}{RX}{receiver}
\newacronym{sdr}{SDR}{software-defined radio}
\newacronym{usrp}{USRP}{Universal Software Radio Peripheral}
\newacronym{lo}{LO}{local oscillator}
\newacronym{mmwave}{mmWave}{millimeter-wave}
\newacronym{eirp}{EIRP}{effective isotropic radiated power}

\newacronym{csma}{CSMA}{carrier-sense multiple access}
\newacronym{csmaca}{CSMA/CA}{carrier-sense multiple access with collision avoidance}
\newacronym{csmacd}{CSMA/CD}{carrier-sense multiple access with collision detection}
\newacronym{mac}{MAC}{medium access control}
\newacronym{phy}{PHY}{physical layer}

\newacronym{4g}{4G}{fourth generation}
\newacronym{lte}{LTE}{Long-Term Evolution}
\newacronym{5g}{5G}{fifth generation}
\newacronym{nr}{NR}{New Radio}
\newacronym{5gnr}{5G NR}{5G New Radio}
\newacronym{ieee}{IEEE}{Institute of Electrical and Electronics Engineers}
\newacronym{lan}{LAN}{local area network}
\newacronym{wlan}{WLAN}{wireless local area network}
\newacronym{bs}{BS}{base station}
\newacronym{ue}{UE}{user equipment}
\newacronym{ul}{UL}{uplink}
\newacronym{dl}{DL}{downlink}
\newacronym{qos}{QoS}{Quality of Service}
\newacronym{fcc}{FCC}{Federal Communications Commission}
\newacronym{iab}{IAB}{integrated access and backhaul}
\newacronym{hetnet}{HetNet}{heterogeneous network}

\newacronym{siso}{SISO}{single-input single-output}
\newacronym{mimo}{MIMO}{multiple-input multiple-output}
\newacronym{sumimo}{SU-MIMO}{single-user \gls{mimo}}
\newacronym{mumimo}{MU-MIMO}{multi-user \gls{mimo}}
\newacronym{ula}{ULA}{uniform linear array}
\newacronym[\glslongpluralkey={angles of arrival}]{aoa}{AoA}{angle of arrival}
\newacronym[\glslongpluralkey={angles of departure}]{aod}{AoD}{angle of departure}
\newacronym{dof}{DoF}{degrees of freedom}
\newacronym{csi}{CSI}{channel state information}
\newacronym{csit}{CSIT}{\gls{csi} at the transmitter}
\newacronym{csir}{CSIR}{\gls{csi} at the receiver}
\newacronym{cs}{CS}{compressed sensing}

\newacronym{elf}{ELF}{extremely low frequency}
\newacronym{slf}{SLF}{super low frequency}
\newacronym{ulf}{ULF}{ultra low frequency}
\newacronym{vlf}{VLF}{very low frequency}
\newacronym{lf}{LF}{low frequency}
\newacronym{mf}{MF}{medium frequency}
\newacronym{hf}{HF}{high frequency}
\newacronym{vhf}{VHF}{very high frequency}
\newacronym{uhf}{UHF}{ultra high frequency}
\newacronym{shf}{SHF}{super high frequency}
\newacronym{ehf}{EHF}{extremely high frequency}
\newacronym{thf}{THF}{tremendously high frequency}
{% endhighlight %}

