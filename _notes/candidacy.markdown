---
layout: note
title: "Advancing to Candidacy for WNCG Students"
categories: []
date: 2021-11-11
summary: Outlining the process for advancing to Ph.D. candidacy in ECE at UT Austin.
published: true
---

This will outline the steps for advancing to candidacy and conducting a Progress Review at UT Austin. 
These steps are based on my experience and are likely most applicable to ECE students in the DICE track, especially those in WNCG.

There are no guarantees on the correctness of any of this nor if your advisor will want you to follow this explicitly. 
When in doubt, do whatever your advisor says and/or whatever Melanie Gulick says, not what I say!

### 1. Talk with Your Supervisor

The first step for advancing to candidacy is discussing such with your supervisor(s) and getting their approval to do so.


### 2. Identify Committee Members

If you have one supervisors, you need at least four committee members. 
1. Supervisor
1. Committee Member 1
1. Committee Member 2
1. Committee Member 3
1. Committee Member 4

If you have two supervisors, you need at least three committee members.
1. Supervisor 1
1. Supervisor 2
1. Committee Member 1
1. Committee Member 2
1. Committee Member 3

Committee members can come from UT ECE, other departments at UT, other universities, or industry. Consider professors that will be able to provide good feedback on your research and/or know you and your research.

Typically, only one member from industry is on a committee, if any at all. 

In general, all committee members need to hold a Ph.D., including those from industry.

> Any professor who is an Assistant, Associate or full Professor in their own department at UT-Austin (from ECE or from another department) is a member of their department's Graduate Studies Committee (GSC), so yes, they are GSC members.  Computer Science professors, or any professors from another department (not ECE) who are guest members of ECE's Graduate Studies Committee are not considered "outside" members of an ECE Dissertation Committee. Link to GSC lists (ECE list is under Electrical Engineering): [https://utdirect.utexas.edu/apps/ogs/auth/gsc/nlogon/gsc_members/](https://utdirect.utexas.edu/apps/ogs/auth/gsc/nlogon/gsc_members/). --- Melanie Gulick


### 3. Contact Your Committee Members

Contact each of your committee members and ask them if they will serve on your committee.

Provide them a brief summary of your research and offer to meet with them to discuss if they wish.


### 4. Collect Completed Candidacy Evaluation Forms

Each non-supervising committee member must complete the [ECE PhD Candidacy Evaluation Form](/pdf/candidacy/ECE-Candidacy-Evaluation-Form-2018.pdf).

Collect these forms from each committee member, sign them yourself, and then get them signed by your supervisor(s). 

Then, email them as attachments to Melanie Gulick.


### 5. Wait for Melanie's Response

Once Melanie has received your forms, she will initiate your candidacy application internally.

Then, she will send you an email with instructions.

After this, you will be able to complete your candidacy application online. You cannot do so until she initiates it internally.


### 6. Draft Dissertation Description

Before beginning the application, draft a description of your dissertation in plain-text (use Notepad on Windows, for example).

This description should be 4600 characters or less (including spaces).

It should include the following:

- the topic of your research
- why you are undertaking the research and what questions you propose to address
- the methodology you intend to use to study/address these questions

This is merely used for internal purposes and is not published anywhere as far as I am aware.


### 7. Complete Online Candidacy Application

Complete the online candidacy application at [https://utdirect.utexas.edu/ogs/forms/candidacy/app.WBX?intro_type=D](https://utdirect.utexas.edu/ogs/forms/candidacy/app.WBX?intro_type=D).

Monitor who has approved your application to make sure that your supervisor(s) are not preventing you from advancing to candidacy.


### 8. Send CVs of External Members

Collect the CV of each external committee member. 

Then, email the CVs to GradStudentSvcs@austin.utexas.edu. 

When you send the email the CV attached:
- address it to "Dear Degree Evaluators"
- include your full name and EID
- explain that you are an ECE graduate student applying for admission to doctoral candidacy
- attach the CV of any external member

See below for instructions.

> Please note that in addition to the online application, you may will need to submit via <del>Main 101 (in the Clock Tower building, one level up from the ground floor)</del> GradStudentSvcs@austin.utexas.edu the CVs of any committee members who are not Assistant, Associate or full Professors at UT-Austin.  This includes external committee members and members who work for UT as researchers, for example, but who are not tenure-track professors.  When you send the email the CV attached, address it to Dear Degree Evaluators, include your full name and EID, explain that you are an ECE graduate student applying for admission to doctoral candidacy, and attach the CV of any external member. --- Melanie Gulick


### 9. Enroll in the Dissertation Course

The semester you enter candidacy and each semester thereafter, you must enroll in the Dissertation course until graduation.

> Please be aware that leaves of absence are RARELY approved for students in candidacy even if you move away, have financial difficulties, etc.  You must register for Dissertation in the long semesters until you finish.  Again, you must be registered in summer if you are graduating in summer, and for the reasons listed above under "summer Registration". --- Melanie Gulick

You may register for Dissertation in summer.

You must register for Dissertation in summer if you are:
- appointed as a GRA, TA, etc. in summer
- planning to graduate in summer 
- meeting regularly with your supervisor and using UT facilities/equipment during summer; there are insurance liabilities that require you to be registered in case you get hurt while using UT equipment


### 10. Schedule Your Progress Review

Ask your supervisor(s) if they have any travel dates or broad unavailability this semester to avoid such conflicts outright.

Then, choose a week you want to conduct your Progress Review.

Choose a few weekdays (ideally in a row) and draft several two-hour times slots. Include up to around 15 time slots across the few days.

Create a Google Form for them to fill out indicating (checkboxes) which time slots work for them. Separate the time slots into different days and include an option for "None / Unvailable" on each day.

{% include image.html file="candidacy/schedule.png" width=60 caption="Something like this." %}

At the top of the form, have committee members select their name so you can identify which member submitted which response.

Send an email with a link to the form to your committee members. Do this ASAP!

Once they have all responded, immediately choose a time that works for everyone. Email them and send them calendar invitations for that time slot.


### 11. Reserve a Conference Room

Reserve a conference room (usually on the sixth or seventh floors).

While your Progress Review may last 2 hours, it may be a good idea to reserve the room with 30 minutes of padding at the start and end.

Include the room in the calendar invitation to your committee members!

Ahead of time, make sure you have card access to the conference room and sort out any audio/visual issues. Make sure the auto-brightness feature of the TV is disabled!


### 12. Schedule a Zoom Meeting (if applicable)

Schedule a Zoom meeting ahead of time if applicable. Ensure settings are appropriate to avoid issues with waiting room, password, etc.

Include the Zoom link in the calendar invitation to your committee members!


### 13. Prepare the Progress Report

Prepare a report of your Ph.D. progress. 

> The student also must provide to the Dissertation Committee members, at least one week in advance of the oral Progress Review, a written Progress Report highlighting the student’s research accomplishments to date, ongoing research, and planed research toward completion of their dissertation. (Emailed pdf versions are acceptable unless otherwise requested.) The body of the report should be brief, 25 double-spaced 12-point font pages including figures and captions maximum. Moreover, students may obtain greater brevity in the discussion of research accomplishments to date by including existing publications in an appendix, with only a corresponding short description of that research—such as provided by the abstract of the publication—in the body of the Progress Report, along with clarification of the student’s contribution to each publication and how that work relates to the topic of their PhD Dissertation. The report also should contain a proposed timeline for completion of their research, and their proposed PhD Program of Work, including previously taken Program of Work courses with grades, and any current and future proposed Program of Work courses.

A good outline is as follows:
- Title page
- Abstract
- Table of contents
- Introduction
- Background
- Brief summary of Contribution 1
- Brief summary of Contribution 2
- ...
- Brief summary of Contribution N
- Ongoing and future work
  - Brief summary of Contribution N+1
  - Brief summary of Contribution N+2
- Timeline to graduation
- Program of work (simply list courses)
- List of publications (and any references)
- Appendices
  - Major publication related to Contribution 1
  - Major publication related to Contribution 2
  - ...
  - Major publication related to Contribution N

Without appendices, the report should be only around 18-25 pages.


### 14. Email Progress Report

At least one week before your Progress Review, email your Progress Report as a PDF to your committee members.


### 15. Invite WNCG-ers to Your Progress Review

Send an email to the WNCG students list inviting them to your Progress Review. 

Include the following in your email:
- date and time
- location
- Zoom link (if applicable)
- title
- abstract


### 16. Choose a Chair for Your Progress Review

Choose one of your committee members to serve as the Chair of your Progress Review. Perhaps make sure they are comfortable with this.

> The Dissertation Supervisor and, if any, Co-Supervisor may attend but neither vote on nor chair the Progress Review. An ECE tenure or tenure track faculty member must be eligible and designated to serve as the Chair of the Progress Review.


### 17. Progress Review Forms

To your Progress Review, print out and bring the following two forms with you:
1. [Program of Work](/pdf/candidacy/Program_of_Work.pdf)
1. [Report of the Progress Review by the ECE Ph.D. Dissertation Committee](/pdf/candidacy/Progress_Review_Report_of_ECE_Dissertation_Committee_12-22-2020_fillable.pdf)

Complete as much of these forms as you can so that the Chair of your Progress Review does not have to.


### 18. Print Out and Email Slides

To your Progress Review, print out and bring a copy of your slides for each committee member (max of 3 slides per page).

Also, 10 minutes before your Progress Review, email a copy of your slides (as PDF) to your committee in case any are attending virtually. This will also provide them a good reminder.


### 19. Conduct the Progress Review

Conduct the Ph.D. Progress Review for your committee.

> ECE graduate students enrolled for the first time in the ECE graduate program in fall 2018 or later, and ECE graduate students enrolled for the first time in the ECE graduate program earlier than fall 2018 who choose to follow the fall-2018-or-later PhD Rules and Procedures, must complete a Progress Review after getting admitted to candidacy and before defending the dissertation and graduating.  A student should take the Progress Review prior to the 9th long semester after attempting 33 hours of coursework while within the Graduate Program, excluding approved leaves of absence. (Leaves of absence after admission to doctoral candidacy are rarely approved, and only for unusual and significant circumstances.) The Progress Review is an oral examination performed before the PhD Dissertation Committee as a whole, addressing the student’s research progress to date and planned research toward completion of their PhD Dissertation, as well any apparent deficiencies in foundational knowledge exhibited by the student during the discussion of their research. --- Melanie Gulick

The target presentation length should be around 50-60 mins.

During the presentation, committee members may and will likely ask questions. 

After the presentation, the committee will first field questions from the audience. Then, the audience will be asked to leave for the closed session with only the committee members. During this, they will ask questions.

Good luck!


### 20. Deliver Progress Review Forms to Melanie

Deliver the two completed forms to Melanie in-person or via email.


### 21. Summarize Feedback (Optional)

Immediately following your Progress Review, document each question, concern, and major feedback offered by your committee during and after the presentation.

Then, for each of these, provide a (polite and formal) response addressing their questions/concerns or summarizing the outcome of their feedback.

Include a cover letter thanking the committee for their time and feedback.

Email this document to the entire committee. Thank them for their time and to let you know if they have any additional feedback or concerns.

This will act as a written record of what needs to be addressed before graduation.
