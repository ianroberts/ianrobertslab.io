---
layout: note
title:  "My LaTeX Workflow"
categories: [latex]
date: 2019-08-20
summary: I provide an overview of the LaTeX practice that I have found to be productive.
published: false
---

### A glossary file

I use a glossary file as shown below. This handles any acronymns I use. Anytime I need a new acronym, I simply add it to this file, which is shared across most, if not all, of my projects.

Then, when I want to use an acronmym, I can call `\gls{snr}` which will display `signal-to-noise ratio (SNR)` the first time it's called and `SNR` anytime after. This may seem like a little excessive, but it makes sure that each acronym used is properly defined and it allows you to update all uses of the acronym if you wish. If you are writing a long document and want to include a list of the acronyms used, this package takes care of that too. 

It can be annoying to type `\gls{snr}` every time, so I often define macros like `\snr` so that I can more naturally call the acronym. 

{% highlight tex %}
\usepackage{xspace}
\usepackage[acronym,nogroupskip,nonumberlist,nopostdot]{glossaries}
\makeglossaries

\newacronym{snr}{SNR}{signal-to-noise ratio}
\newcommand{\snr}{\gls{snr}\xspace}
{% endhighlight %}


### Math Macros

I also use a math macros file like the one shown below. This makes typing math much easier and consistent.

For example, I define the conjugate tranpose operation as shown below to be a superscript `H`. Typing `\ctrans` is a lot faster and is more readable than `^{\textsf{H}}}`. Additionally, it is common to use a superscript asterisk  `*` rather than a superscript `H` to denote the conjugate transpose. If I wanted to change all uses to be the asterisk notation, it would be much easier to edit this single macro rather than update every use of it. 

I also define macros like `\mat{}` which properly format a symbol in matrix notation. For example, I use `\mathbf{}` as my font for matrix variables, so I define `\mat{}` to apply this font to its argument. Again, the advantage here is consistency and ease of updating should I ever change notation.

Fianlly, I like to define variables in this file. For example, I use the letter `F` to denote a MIMO precoder so I define a macro `\pre` to call the precoder symbol. Again, this is nice for consistency, ease of updating, and readability.

`\ensuremath{}` is used to ensure that its argument is typeset in math mode. This makes writing inline math a little faster. There is an issue, though, with spacing afterward so I use `\xspace` to put a space when there should be one and to not when there shouldn't (i.e., before punctuation).

{% highlight tex %}
\usepackage{amssymb}
\usepackage{xspace}

\newcommand{\ctrans}{\ensuremath{^{\textsf{H}}}\xspace} % conjugate transpose of a matrix
\newcommand{\mat}[1]{\ensuremath{\mathbf{#1}}\xspace} % matrix notation (bold)
\newcommand{\pre}{\ensuremath{\mat{F}}\xspace} % precoder symbol
{% endhighlight %}

### main.tex

For my main TeX file, I try to keep it clean, something like what's shown below, which is for an IEEE paper. Notice that I use `\input{}` quite a lot. I like this because I can simply comment out things and create new versions if I want. Notice I have commented out several things and am on the second version of the introduction. And it allows me to find what I'm looking for much easier.

I name any sections with the prefix `sec-`. I also store tables and some equations in their own `.tex` files for which I use the prefixes `tab-` and `eq-`. This keeps things organized.

I have two `\documentclass`es because IEEEtran comes with a draft mode so it's easier to toggle between the draft and final formats.

Also, I use `\glsresetall` to reset my acronyms after the abstract. This is optional. Sometimes I use it, sometimes not.

{% highlight tex %}
\documentclass[conference,twocolumn,10pt]{IEEEtran}
%\documentclass[journal,onecolumn,10pt,draftcls]{IEEEtran}

\input{sec-preamble.tex}

\begin{document}
  \input{sec-title.tex}
  \input{sec-abstract.tex}
  % \input{sec-keywords.tex}
  \input{sec-peer-review-title.tex}
  \glsresetall
  \input{sec-introduction-v2.tex}
  \input{sec-system-model.tex}
  \input{sec-bfc-v4.tex}
  \input{sec-simulation-results.tex}
  \input{sec-conclusion.tex}
  % \appendix
  % \input{sec-appendices.tex}
  % \input{sec-acknowledgement.tex}
  \input{sec-bibliography.tex}
  % \input{sec-biography.tex}
\end{document}
{% endhighlight %}


### Preamble

In my preamble, I like to include the following lines. I use common files (bibliography, glossary, math) since most of what I write is related.

{% highlight tex %}
\usepackage[natbib=true,style=ieee,backend=bibtex,useprefix=true,maxcitenames=3]{biblatex} % IEEE format
\addbibresource{../../latex/refs_2.bib} % a common bibliography for my LaTeX projects to share
\usepackage[linesnumbered,ruled]{algorithm2e} % to include algorithms in papers

\input{../../latex/mymath.tex} % a common mymath.tex file for my LaTeX projects to share
\input{../../latex/glossary.tex} % a common glossary.tex file for my LaTeX projects to share

\newcommand{\figref}[1]{Fig.~\ref{#1}} % for consistent referencing of figures
\newcommand{\algref}[1]{Algorithm~\ref{#1}} % for consistent referencing of algorithms
{% endhighlight %}


### Figures

For all figures, I use `.eps` or `.pdf`. These are vector-based graphics so they won't pixelate (in most cases). Always use vector graphics if at all possible. Bitmaps are not appropriate for most technical documents in my opinion. They look horrible. 

I use [LaTeXDraw](http://latexdraw.sourceforge.net/) to create graphics. It takes some getting used to and isn't perfect, but I have grown to like it pretty well. It runs on Java so it can run on Windows or Ubuntu. Below are a couple of figures I made in LaTeXDraw.

![Two nodes connected to a full-duplex base station.](/images/mmwave/nodes.svg#center, "Two nodes connected to a full-duplex base station")

![Hybrid beamforming mmWave communication block diagram.](/images/mmwave/trx.svg#center, "Hybrid beamforming mmWave communication block diagram.")

In MATLAB, I export figures as `.eps` using the following. If possible, be sure to use appropriate line styles and markers so that they can be read in black and white.

{% highlight matlab %}
% Options
save_plots_eps = true;

% Save directory
save_dir = 'plots/';
if save_plots_eps
    mkdir(save_dir)
end

% Plot
figure(1);
plot(x,y,'-k'); grid on;
xlabel('$x$');
ylabel('$y$');
file = 'my_figure';
if save_plots_eps
    print([save_dir file],'-depsc');
end
{% endhighlight %}

To get MATLAB to render LaTeX math and text, I put the following in my `startup.m` file. You could also copy this and run in the MATLAB Command Window.

{% highlight matlab %}
% Plot settings
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');
set(groot, 'defaultAxesFontSize', 12);
set(groot, 'defaultColorbarTickLabelInterpreter','latex'); 
{% endhighlight %}

Below is one of my plots. See how the labels and legend are in Computer Modern font. Also, the lines could be differentiated even if this plot was in black and white. Also, don't include a title -- your caption in TeX should describe the figure. I like to have `grid on;` also. I only capitalize the first word of the labels (and any acronyms). Include the units in lowercase and in parentheses. Also, be sure to set the location of the legend using `'Location','Northwest'`, for example.

![Comparing the number of RF chains in BFC.](/images/mmwave/comparing_nrf.svg#center, "Comparing the number of RF chains in BFC.")
