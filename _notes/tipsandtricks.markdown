---
layout: note
title:  "Tips and Tricks I Find Helpful"
categories: [other]
date: 2020-01-30
summary: A list of various tips and tricks I have found to be helpful, primarily related to my time as a graduate student. Perhaps some of it works for you.
---

### Finding/Choosing a Research Problem

- Finding a research problem can be very difficult. I found mine quite naturally, luckily, while I was sitting in class one day. I didn't end up pursuing that topic until my advisor encouraged me to about two months later. I didn't initially pursue it because I didn't think too much of it at the time---nothing more than a curiosity. I suppose the lesson learned is to pursue interesting ideas unless you find a good reason not to.
- I keep a list of research ideas. If something pops into my head, I write it down or else I will forget it. I come back to this list occasionally to reevaluate what I am working on and to determine what's up next.
- When deciding what to work on, I try to balance what I'm most excited about and what should/needs to be worked on.


### Getting Started on a Research Problem

- I like to write out my thoughts on my tablet (with stylus) to develop them. I am often surprised by how clear (and how muddy) some ideas become when I force myself to write them out. 
- Often times, formulating an idea with math really helps develop my ideas.
- A good motivator for me to begin making progress is to target an upcoming paper deadline. I'm often surprised with how much progress I can make once I have something to shoot for.


### Writing Papers, Preparing Slides, and Creating Figures

- I write almost exclusively in LaTeX. I use Beamer for my slides.
- My LaTeX editor of choice is [TeXstudio](https://www.texstudio.org/). After experiencing some issues with my TeXstudio installation on Ubuntu, I found that [this installation script](https://github.com/scottkosty/install-tl-ubuntu) solved my issues.
- [Overleaf](https://www.overleaf.com/) is a great option too, but I prefer working from TeXstudio. When I collaborate, I'll typically use Overleaf along with [its Git integration](/notes/latex/overleafgit/) so I can work locally and push my changes periodically.
- My graphics editor of choice is [LaTeXDraw](https://github.com/latexdraw/latexdraw). It's not perfect but I have grown accustomed to it. I have looked around for other solutions but none do what I want as well as LaTeXDraw.
- I like to develop my ideas by writing them down and then creating slide decks on them.


### Technology

- I primarily use [Ubuntu](https://ubuntu.com/) 18.04. I occasionally use Windows 10.
- I use Git ([GitLab](https://gitlab.com/)) to sync all files across my personal desktop, personal laptop, and lab desktop(s). I create repositories for projects, papers, etc. I also have a "master" repo that I use for things that don't really deserve their own repo. Even when I'm away from my devices, I can always log in to GitLab to access my files. It gives me incredible peace of mind knowing my files are safe and always accessible.
- I use a [Samsung Galaxy Tab S6](https://www.samsung.com/us/mobile/tablets/tab-s6/) and its stylus for taking notes, writing out ideas, reading and annotating papers, etc.
- On my Tab S6, I find the following apps to be especially useful: [Samsung Notes](https://play.google.com/store/apps/details?id=com.samsung.android.app.notes&hl=en_US), [Squid](https://play.google.com/store/apps/details?id=com.steadfastinnovation.android.projectpapyrus&hl=en_US), and [Google Drive](https://drive.google.com).
- I do a lot of writing (typing) so I have invested in a good keyboard after experiencing some hand discomfort. I use the [Logitech G513 keyboard](https://www.logitechg.com/en-us/products/gaming-keyboards/g513-backlit-mechanical-gaming-keyboard.html) and the [Logitech MX Master 2S mouse](https://amazon.com/Logitech-Master-Wireless-Mouse-Rechargeable/dp/B071YZJ1G1).
- I subscribe to automatic alerts from [EDAS](http://edas.info) based on my areas of interest. I am sent emails quite frequently for calls for papers.
- I use Google Drive to sync lots of things. The Google Drive app also has document scanning capability making it convenient to digitize hand-written homework and notes.


### Staying Productive

- I always think I could be better at this. I haven't found the secret formula for this yet, but have found things that work for me.
- I have found it's a good idea to take advantage of motivation before it fades. When deadlines aren't immediate, I like to work on tasks that are both productive and fun to me. At some point, though, I eventually have to work on those items that don't sound quite so fun.
- I have tried to invest in a workspace that encourages productivity. I bought a goood mouse and keyboard. I have two monitors. I have an arm mount for my monitors, which frees up desk space. I have comfortable headphones. I have a decent chair. I have a good sized desk. It is important to me to encourage productivity even if it means spending a little bit of money.
- I keep a weekly to-do list. Every Sunday, I list out the week's tasks and their deadlines. I add to the list as the week progresses. (See [Bullet Journaling](https://bulletjournal.com/).)

