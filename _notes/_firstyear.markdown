---
layout: note
title:  "My First Year of Grad School"
categories: [gradschool]
---

> In this note, reflect on some aspects of my first year in grad school.

### First Few Weeks

My first class was Wireless Communications with Jeff Andrews. The class was relatively large (around 30 students), though I didn't take too much notice at first since I sat in the front row. I was ready to learn from *the* Jeff Andrews but was scared about what lied ahead. I immediately returned to "school" mode and tried hard to do my best. 

My other class was Probability with Shakkottai. Most students in my area (known as "DICE" at UT) take this class their first semester of grad school. In my opinion, above all, this class introduces graduate students to a more advanced way of mathematical thinking (at least from my perspective). I knew it would be a tough class, so I just tried my best. I wasn't particularly interested in probability, so I was more going through the motions in this class so that I could focus more on Jeff's, to be honest.

Overall, my first few weeks were fast-paced, especially since I was continuing my work at GenXComm while juggling classes.


### Fall Semester

Classes went on. I had many "grad school thoughts" as I like to call them. You know, questions, observations, and realizations you have about the grad school experience.

The most frequent thoughts I had were regarding my abilities, especially compared to my peers. I would often compare myself to those around me and wonder where I stood relative to them. As you can imagine, this isn't the healthiest of habits. Pretty much everyone in WNCG is high-calliber in my opinion. Many U.S. students come from top U.S. schools. Most international students come from the best schools in their respective countries. Being a first-year, I was surrounded by students having much more experience than me. Even students with just one eyar of grad school under their belt have a big leg up on first-years --  you learn a lot in your first year. Comparing myself to those around me, then, was not really fair to myself. It took time to figure this out, and perhaps I still haven't fully got it under control. 

In academia, there is a sort of fame that exists. Those landmark papers, the professors that crank out tons of impactful work -- that sort of fame. I know a lot of people notice this "academic fame" but keep in mind that this is all from my perspective. Coming from a smaller university, I wasn't exposed to this fame firsthand. My undergrad advisor is an IEEE Fellow, a high honor, but to be face to face with some "famous" professors was difficult for me to ignore. This led me to question if I had what it took to get to such a level. Could I be the next Jeff Andrews? Could my work be highly impactful? Will my work be highly impactful? I wasn't sure if I wanted to go into academia, but that didn't matter. I wanted to be confident that I *could* be academic famous if I wanted to. It probably sounds a little ridiculous. I don't like restricting myself, though, so accepting that I couldn't be academically famous would be difficult for me to do. Anyhow, I think I got over this by convincing myself to work hard and be patient. To get to any sort of academic fame, it would require hard work. There is no sense in wondering what will happen unless I work hard. So, I might as well just work hard now. I also told myself that it takes time to get to any sort of academic fame. Writing a high-impact paper, for example, requires years of knowledge and foundation. Solving difficult problems requires a lot of experience, so I focused on building my skills.

I suppose I should mention that I don't know if I necessarily want to be academically famous. I would love for my future papers to be high-impact contributions, but I'm not sure if academia is the route I will take after graduation. Parts of me think I would be good at being a professor, but I am still not sure if it would be a good fit. Luckily, I am much less affected by this academic fame complex I had early on. I think it is largely because I trust that I will end up doing just fine. 

I didn't do much research this semester because most of my time was spent doing work at GenXComm and on my Wireless class project. I had chosen Digital Self-Interference Cancellation as my topic. I didn't know any thing about D-SIC, but knew some about analog SIC from my work at GenXComm. Luckily, Rajesh steered me toward some good resources and provided me help. I learned a lot technically, but more importantly, I learned how to independently research a technical topic. It wasn't easy for me to organize my thoughts, sort through papers, and communicate my findings, but I gained a lot of confidence after forcing myself to do so.

### Winter Break

I relaxed in Missouri for the first couple of weeks of break. I spent some time trying to think on possible research topics, but didn't get very far. When I returned to Austin, I met with Sriram and Hardik. They introduced the topic if integrated access and backhaul to me as a possible research direction. I liked it, so I immediately got started looking into it. I was excited to have a research topic I could work on and was interested in.

### Spring Semester


