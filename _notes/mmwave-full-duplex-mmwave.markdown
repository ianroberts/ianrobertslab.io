---
layout: note
title:  "Millimeter Wave Communication Systems"
categories: [fdx]
tags: fdx
date: 2021-06-27
summary: A brief introduction to mmWave communication systems.
published: false
---


*This post is part of [A Series on Millimeter Wave Full-Duplex](/notes/mmwave-full-duplex-series).*


### Why Operate at Such High Frequencies?

Millimeter wave (mmWave) transceivers operate at carrier frequencies in and around 30 GHz to 100 GHz.
Compared to conventional sub-6 GHz carrier frequencies used for wireless communication, there is significantly more spectrum available in mmWave frequency bands, meaning wider bandwidths can be leveraged to net higher data rates and support more users.


### Overcoming Path Loss with Dense Antenna Arrays

Generally, the path loss experienced by propagating electromagnetic waves increases with frequency (often quadratically or more).
As a result, mmWave signals typically suffer from severe path loss.
To combat this, mmWave transceivers rely on many antennas (often dozens or hundreds) to produce high beamforming gain: with more antennas, higher gain can be delivered.
Leveraging dense antenna arrays at a transmitter and receiver can establish enough link margin to sustain communication.
Fortunately, these dense arrays do not occupy prohibitively large footprints, courtesy of the fact that resonant antenna size decreases as wavelength decreases; in fact, the two-dimensional footprint of an array decreases quadratically with wavelength, making mmWave communication even more plausible.
It is worth noting that there exist some applications in communications as well as sensing where the high path loss faced by mmWave signals can be useful in reducing interference.


### Digital Beamforming

In sub-6 GHz multiple-input multiple-output (MIMO) communication systems, multi-antenna radios are typically fully-digital, meaning they have a dedicated transmit chain and receive chain per-antenna.
This means the signal at each antenna can be independently processed before transmission or after reception.
In other words, the signals can be processed digitally in baseband before undergoing digital-to-analog conversion and upconversion on the transmit side or after analog-to-digital conversion and downconversion on the receive side.


### Analog Beamforming

Multi-antenna mmWave transceiver architectures differ from their conventional sub-6 GHz counterparts as a result of the many more antennas present.
It is prohibitively costly in terms of power consumption, financial cost, and size to have a dedicated transmit and receive chain per-antenna since it is not uncommon to have upwards of 64 to 256 antennas in a mmWave array.
Instead, one mmWave architecture is to use analog beamforming, where a network of phase shifters and possibly attenuators can beamforming a transmit or receive signal with proper configuration.
These phase shifters and attenuators are typically controlled digitally in practice, meaning they are subject to some phase/amplitude resolution.
In literature, it often assumed there is a lack of amplitude control (only phase shifters), though it is not uncommon to also have digitally-controlled attenuators in practice.


### Hybrid Digital/Analog Beamforming

{% include image.html file="mmwave/mmwave_hybrid_beamforming_block.svg" caption="Hybrid digital/analog beamforming." %}


### Comparing Analog Beamforming and Hybrid Beamforming


### Useful References
- 
