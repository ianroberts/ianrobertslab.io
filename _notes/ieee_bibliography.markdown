---
layout: note
title:  "IEEE Bibliographies in LaTeX"
date: 2020-11-16
categories: [latex]
summary: Tips and tricks for bibliographies in IEEE papers.
---

### Preliminaries

The `IEEEtran` LaTeX styling files come with BibTeX files that will automatically format your bibliography according to IEEE format.

In the boilerplate `IEEEtran` files, uncomment the following to use the `cite` package.
{% highlight latex %}
\usepackage{cite}
{% endhighlight %}


To print your bibliography use the following, where `refs.bib` is my BibTeX file containing my reference entries. Note that you will have to have called `\cite{ENTRY_NAME}` within your document for this to populate your bibliography.

{% highlight latex %}
\bibliographystyle{bibtex/IEEEtran}
\bibliography{bibtex/IEEEabrv,refs}
{% endhighlight %}


### Example Journal/Magazine (Periodical) Citation

Below is the basic format for citing a periodical such as journal or magazine.

> J. K. Author, “Name of paper,” Abbrev. Title of Periodical, vol. x, no. x, pp. xxx-xxx, Abbrev. Month, year.


### Example Conference Paper Citation

Below is the basic format for citing a conference paper.

> J. K. Author, “Title of paper,” in Abbreviated Name of Conf., (location of conference is optional), (Month and day(s) if provided) year, pp. xxx-xxx.


### Abbreviations File

In addition to `bibtex/IEEEtran`, the IEEE LaTeX styling files also come with `bibtex/IEEEabrv`, which is an abbreviations file containing the preferred abbreviations for a variety of IEEE publications.

For example, it contains the following, which maps the identifier `IEEE_J_WCOM` to the abbreviation `IEEE Trans. Wireless Commun.`.

{% highlight latex %}
@STRING{IEEE_J_WCOM = "{IEEE} Trans. Wireless Commun."}
{% endhighlight %}

You are free to create your own custom abbreviations as needed. Some custom abbreviations I have used the following.

IEEE actually provides some recommended abbreviations here [http://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf#page=18](http://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf#page=18).



#### IEEE Conferences

{% highlight latex %}
@STRING{IEEE_C_GLOBECOM   = "Proc. {IEEE} Global Commun. Conf."} % (GLOBECOM)
@STRING{IEEE_C_ICC        = "Proc. {IEEE} Intl. Conf. Commun."} % (ICC)
@STRING{IEEE_C_ISIT       = "Proc. {IEEE} Intl. Symp. Inf. Theory"} % (ISIT)
@STRING{IEEE_C_RADAR      = "Proc. {IEEE} Intl. Radar Conf."} % 
{% endhighlight %}


#### Other Conferences

{% highlight latex %}
@STRING{ACM_C_MOBICOM      = "Proc. {ACM} Intl. Conf. Mobile Comput. and Netw. (MobiCom)"} % 
@STRING{ACM_C_SIGCOMM      = "Proc. {ACM} SIGCOMM Conf."}
@STRING{MISC_C_ISWCS       = "Proc. Intl. Symp. Wireless Commun. Syst."} %  (ISWCS)
@STRING{MISC_C_ITA         = "Proc. Inf. Theory and its Appl."} % (ITA)
@STRING{MISC_C_ASILOMAR    = "Proc. Asilomar Conf. Signals, Sys., and Comput."}
@STRING{MISC_C_USENIX_NSDI = "Proc. {USENIX} Conf. Netw. Sys. Design and Impl. (NSDI)"}
{% endhighlight %}


#### Workshops

{% highlight latex %}
@STRING{IEEE_W_GLOBECOM   = "Proc. {IEEE} Global Commun. Conf. Wkshp."} % (GLOBECOM Workshops)
@STRING{IEEE_W_ICC        = "Proc. {IEEE} Intl. Conf. Commun. Wkshp."} %  (ICC Workshops)
@STRING{IEEE_W_WIMESH     = "Proc. {IEEE} Wkshp. Wireless Mesh Netw."} % IEEE Workshop on Wireless Mesh Networks
@STRING{IEEE_W_SPAWC      = "Proc. {IEEE} Intl. Wkshp. Signal Proc. Adv. Wireless Commun."} 
{% endhighlight %}


### Example Journal BibTeX Entry

An example journal paper's BibTeX entry is below. To cite this in a document, I would simply call `\cite{heath_overview_2016}`. 

While many sources, including IEEE Xplore, provide BibTeX entries for publications, they are often not in proper format and must be tailored one by one to make sure they produce the proper output once compiled.

{% highlight latex %}
@ARTICLE{heath_overview_2016,
	title = {An Overview of Signal Processing Techniques for Millimeter Wave {MIMO} Systems},
	volume = {10},
	number = {3},
	journal = IEEE_J_STSP,
	author = {Heath, R. W. and Gonz{\'a}lez-Prelcic, N. and Rangan, S. and Roh, W. and Sayeed, A. M.},
	month = apr,
	year = {2016},
	pages = {436--453},
}
{% endhighlight %}

Always be sure to use an en dash `--` for page ranges and to use accented characters as needed. Also notice that `{MIMO}` is wrapped in braces in the title to ensure that when displayed it will be uppercase, whereas the rest of the title's capitalization will be properly handled by the IEEE BibTeX file. Note that you can explicitly write the `booktitle` entry rather than using the abbreviations file you'd like.

This will produce the following.

> R. W. Heath, N. González-Prelcic, S. Rangan, W. Roh, and A. M. Sayeed, "An overview of signal processing techniques for millimeter wave MIMO systems," IEEE J. Sel. Topics Signal Process., vol. 10, no. 3, pp. 436--453, Apr. 2016.


### Example Conference BibTeX Entry

An example conference paper's BibTeX entry is below. To cite this in a document, I would simply call `\cite{roberts_2020_fsbfc}`.

{% highlight latex %}
@INPROCEEDINGS{roberts_2020_fsbfc,
	author={Roberts, Ian P. and Jain, Hardik B. and Vishwanath, Sriram},
	booktitle=IEEE_C_ICC, 
	title={Frequency-Selective Beamforming Cancellation Design for Millimeter-Wave Full-Duplex}, 
	year={2020},
	pages= {1--6}
}
{% endhighlight %}

This will produce the following.

> I. P. Roberts, H. B. Jain, and S. Vishwanath, "Frequency-selective beamforming cancellation design for millimeter-wave full-duplex," in Proc. IEEE Intl. Conf. Commun., 2020, pp. 1--6.


