---
layout: note
title: "Beam Alignment"
categories: [wireless,mmwave]
date: 2023-03-15
summary: An introduction to beam alignment in high-frequency communication systems.
published: true
---

Beam alignment (also called "beam management" or "beam training") is a critical component to real-world high-frequency communication systems, like those in 5G and IEEE 802.11ay (60 GHz Wi-Fi).

Closing the link---or establishing a strong connection---between devices is a key challenge in wireless communication systems operating at mmWave (roughly 30 GHz to 100 GHz) and sub-THz (roughly 100 GHz to 1 THz) carrier frequencies.
This stems from the simple fact that path loss generally increases with increased carrier frequencies.
Antenna arrays containing dozens or hundreds of individual antenn elements are used in high-frequency wireless systems to focus energy in a particular direction through what is called _beamforming_.
By focusing energy in this way, received signal power is increased to offset the effects of increased path loss.

Effectively steering a highly directional beam is not a trivial task, however, because it must be steered accurately---a problem that worsens with mobility.
This is known as the "beam alignment" or "beam management" problem.
Slight mis-alignment of beams can lead to dramatic degradations in signal quality, and re-aligning beams can consume valuable radio resources if too frequent.

{% assign f = "/fig/alignment.svg" %}
{% assign c = "Beam alignment is used to close the link between a mmWave base station (BS) and user equipment (UE)." %}
{% include image.html file=f width=50 caption=c link=false %}

Rather than measure a high-dimensional multiple-input multiple-output (MIMO) channel and subsequently configure a beamformer, modern mmWave systems instead rely on beam alignment procedures to identify promising beamforming directions, typically via exploration of a codebook (a set) of candidate beams, as illustrated in above. 
This offers a simple and robust way to configure a beamformer without downlink/uplink MIMO channel knowledge a priori, which is not obtainable in practice.

Executing codebook-based beam alignment would aim to solve (or approximately solve) the following problem or one taking a similar form.

$$\mathbf{f}^{\star} = \arg\max_{\mathbf{f} \in \mathcal{F}} \ \mathsf{SNR}(\mathbf{f}) $$

Here, $$\mathbf{f}$$ is a beam (or a vector of beamforming weights) and $$\mathcal{F}$$ is a set of beams (the codebook).
$$\mathsf{SNR}(\mathbf{f})$$ is the received signal-to-noise ratio (SNR) with beam $$\mathbf{f}$$.
The selected beam $$\mathbf{f}^{\star}$$ is that which maximizes SNR.

Practically, this problem is often solved through a series of over-the-air measurements.
For instance, referring to the figure above, the base station may transmit pilot signals on each beam $$\mathbf{f}$$ in the codebook $$\mathcal{F}$$.
For each, the user measures and records the reference signal received power (RSRP).
Then, it can inform the base station of the beam index that maximized its RSRP.
The base station would then use the reported beam whenever serving that particular user.

The primary advantage of this beam sweeping approach is that it circumvents the need for knowledge of the over-the-air MIMO channel (the channel between the base station and the user).
Instead, received signal power measurements are taken _across_ the channel of interest.

Scaling this concept of beam alignment to multiple users is important in real-world systems.
The beam sweeping approach outlined herein may seem naive, but it is simple, robust, and scales well with many users.
More sophisticated strategies that have been proposed recently typically fall short in robustness or in scaling with many users.


{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
