<div class="postit">
<div class="postit-body" markdown="1">
Thank you for attending my Ph.D. Progress Review!  
A public version of my slides can be found here: [**[PDF]**](/pdf/slides_progress_review.pdf).
</div>
</div>
