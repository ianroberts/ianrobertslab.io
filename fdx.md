---
layout: page
title: Full-Duplex
hide_in_nav: false
categories: fdx
permalink: fdx/
---

> A centralized collection of posts and news on full-duplex, with an emphasis on mmWave full-duplex.


### Calls for Papers in Full-Duplex

{% assign n = (site.data.cfp_fdx | sort: 'date') | reverse %}
{% for cfp in n limit:10 %}
- **[{{ cfp.venue }}]({{ cfp.link }})**  
  Due: {{ cfp.date | date: "%B %d, %Y" }}
{% endfor %}


### Recent Highlights in Full-Duplex

If you have any news items you would like me to (anonymously) include on this page, please fill out this [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSd-prohuqObYHFAmPmavqGo2Wq1EM8Lr7XgEmPQqbsrqW716w/viewform?usp=sf_link). 

{% assign n = (site.data.news_fdx | sort: 'date') | reverse %}
{% for news in n limit:10 %}
**{{ news.date | date: "%B %d, %Y" }}**  
{{ news.text }}
{% endfor %}


### All Posts

{% assign notes = site.fdx | reverse %}
{% for note in notes %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
{% endfor %}

[//]: # test


