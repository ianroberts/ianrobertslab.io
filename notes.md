---
layout: page
title: Notes
categories: notes
permalink: notes/
---

> A collection of posts on various topics that may help you learn something technically or otherwise. Some of them are targeted primarily at prospective and current graduate students in the area of wireless and signal processing.

{% assign notes = site.notes | reverse %}
{% for note in notes %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
{% endfor %}

