---
layout: page
title: mmWave Full-Duplex
hide_in_nav: true
categories: fdx
permalink: fdx/mmwave/
---

> A collection of resources for those interested in in-band full-duplex capability.


### The Problem

### The Solution

### Implementing the Solution

#### Self-Interference Cancellation in Hardware

#### Self-Interference Cancellation in Software/Logic

### Recommended Resources

