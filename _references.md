---
layout: page
title: References
categories: references
permalink: references/
---

> During my undergrad, I found it difficult to find good references for what I wanted to learn. I often found myself searching around on the web, trying to teach myself something new. Many hours were wasted looking around and never really finding sources that communicated the material to me. Since starting grad school, I have been exposed to a lot of good books and resources that have done a much better job presenting technical concepts, often written by experts in their field. Below is a list of materials I have found to be useful.


### Wireless Communication

{% for b in site.data.books.wireless %}

**{{ b.title }}**, _{{ b.author }}_  
{% if b.summary %}{{ b.summary }}  {% endif %}
Level: {{ b.level }}. [<a href="{{ b.link }}" target="_blank">link</a>]
{% endfor %}


### Linear Algebra

{% for b in site.data.books.linalg %}

**{{ b.title }}**, _{{ b.author }}_  
{% if b.summary %}{{ b.summary }}  {% endif %}
Level: {{ b.level }}. [<a href="{{ b.link }}" target="_blank">link</a>]
{% endfor %}


### Compressed Sensing

{% for b in site.data.books.cs %}

**{{ b.title }}**, _{{ b.author }}_  
{% if b.summary %}{{ b.summary }}  {% endif %}
Level: {{ b.level }}. [<a href="{{ b.link }}" target="_blank">link</a>]
{% endfor %}


### Full-Duplex

{% for b in site.data.books.fullduplex %}

**{{ b.title }}**, _{{ b.author }}_  
{% if b.summary %}{{ b.summary }}  {% endif %}
Level: {{ b.level }}. [<a href="{{ b.link }}" target="_blank">link</a>]
{% endfor %}


### Other

{% for b in site.data.books.other %}

**{{ b.title }}**, _{{ b.author }}_  
{% if b.summary %}{{ b.summary }}  {% endif %}
Level: {{ b.level }}. [<a href="{{ b.link }}" target="_blank">link</a>]
{% endfor %}
